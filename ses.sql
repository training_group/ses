/*
Navicat MySQL Data Transfer

Source Server         : 云服务
Source Server Version : 50716
Source Host           : 39.108.157.102:3306
Source Database       : ses

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-10-26 08:40:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Class
-- ----------------------------
DROP TABLE IF EXISTS `Class`;
CREATE TABLE `Class` (
  `Class_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `Class_Name` varchar(12) NOT NULL DEFAULT '' COMMENT '班级名称',
  `Class_Type` varchar(2) NOT NULL DEFAULT '0' COMMENT '班级类型:1:本科，2:专科，3:高职，4:成教',
  PRIMARY KEY (`Class_Id`),
  UNIQUE KEY `unique_className` (`Class_Name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='班级表';

-- ----------------------------
-- Table structure for Class_Course
-- ----------------------------
DROP TABLE IF EXISTS `Class_Course`;
CREATE TABLE `Class_Course` (
  `CC_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `Class_Id` int(11) NOT NULL DEFAULT '0' COMMENT '外键，关联班级表id',
  `C_Id` int(11) NOT NULL DEFAULT '0' COMMENT '外键，关联课程表id',
  PRIMARY KEY (`CC_Id`),
  KEY `class_id_PK` (`Class_Id`),
  KEY `c_id_pk` (`C_Id`),
  CONSTRAINT `c_id_pk` FOREIGN KEY (`C_Id`) REFERENCES `Course` (`C_Id`),
  CONSTRAINT `class_id_PK` FOREIGN KEY (`Class_Id`) REFERENCES `Class` (`Class_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='班级_课程表';

-- ----------------------------
-- Table structure for Course
-- ----------------------------
DROP TABLE IF EXISTS `Course`;
CREATE TABLE `Course` (
  `C_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程ID',
  `C_Name` varchar(20) NOT NULL DEFAULT '' COMMENT '课程名称',
  `C_Type` varchar(2) NOT NULL DEFAULT '0' COMMENT '课程类型（1:必修，2:选修）',
  PRIMARY KEY (`C_Id`),
  UNIQUE KEY `unique_cName` (`C_Name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='课程表';

-- ----------------------------
-- Table structure for Department
-- ----------------------------
DROP TABLE IF EXISTS `Department`;
CREATE TABLE `Department` (
  `D_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门 ID',
  `D_name` varchar(20) NOT NULL DEFAULT '' COMMENT '部门名称',
  PRIMARY KEY (`D_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Table structure for Evaluate
-- ----------------------------
DROP TABLE IF EXISTS `Evaluate`;
CREATE TABLE `Evaluate` (
  `E_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评教ID',
  `E_Title` varchar(100) NOT NULL DEFAULT '' COMMENT '评教标题',
  `E_Parent_Id` int(11) NOT NULL DEFAULT '0' COMMENT '评教父id关联父级id，即本表的E_Id（顶层的父id为0）',
  `type` varchar(2) DEFAULT NULL COMMENT '类型',
  `E_IsEnd` int(11) NOT NULL DEFAULT '0' COMMENT '是否最终项（0否1是），只有最终项才在后面生成评分项',
  PRIMARY KEY (`E_Id`),
  KEY `E_Parent_E_id_PK` (`E_Parent_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='评教表';

-- ----------------------------
-- Table structure for Manager
-- ----------------------------
DROP TABLE IF EXISTS `Manager`;
CREATE TABLE `Manager` (
  `M_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员表ID',
  `M_Account` varchar(20) NOT NULL DEFAULT '' COMMENT '管理员账号',
  `M_Pass` varchar(35) NOT NULL DEFAULT '' COMMENT '管理员密码',
  PRIMARY KEY (`M_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Table structure for Score
-- ----------------------------
DROP TABLE IF EXISTS `Score`;
CREATE TABLE `Score` (
  `Score_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '计分表ID',
  `Score_Num` double(5,2) DEFAULT NULL,
  `T_Id` int(11) DEFAULT '0' COMMENT '教师id ;外键-关联教师id',
  PRIMARY KEY (`Score_Id`),
  KEY `score_t_id_PK` (`T_Id`),
  CONSTRAINT `score_t_id_PK` FOREIGN KEY (`T_Id`) REFERENCES `Teacher` (`T_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='计分表';

-- ----------------------------
-- Table structure for Score_Stu
-- ----------------------------
DROP TABLE IF EXISTS `Score_Stu`;
CREATE TABLE `Score_Stu` (
  `SS_Id` int(11) NOT NULL COMMENT '主键ID',
  `T_Id` int(11) NOT NULL DEFAULT '0' COMMENT '教师id;外键关联教师id',
  `S_Id` int(11) NOT NULL DEFAULT '0' COMMENT '学生id;外键关联学生id',
  PRIMARY KEY (`SS_Id`),
  KEY `Score_Stu_T_id_PK` (`T_Id`),
  KEY `Score_stu_id_PK` (`S_Id`),
  CONSTRAINT `Score_Stu_T_id_PK` FOREIGN KEY (`T_Id`) REFERENCES `Teacher` (`T_Id`),
  CONSTRAINT `Score_stu_id_PK` FOREIGN KEY (`S_Id`) REFERENCES `Student` (`S_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计分_学生中间表';

-- ----------------------------
-- Table structure for Score_Tea
-- ----------------------------
DROP TABLE IF EXISTS `Score_Tea`;
CREATE TABLE `Score_Tea` (
  `ST_Id` int(11) NOT NULL COMMENT '主键，自增',
  `T_Id` int(11) NOT NULL COMMENT '被评教的教师id',
  `E_T_Id` int(11) NOT NULL COMMENT '评教的教师id',
  PRIMARY KEY (`ST_Id`),
  KEY `FK_ScoreTea_T_Id` (`T_Id`),
  KEY `Fk_ScoreTea_E_T_Id` (`E_T_Id`),
  CONSTRAINT `FK_ScoreTea_T_Id` FOREIGN KEY (`T_Id`) REFERENCES `Teacher` (`T_Id`),
  CONSTRAINT `Fk_ScoreTea_E_T_Id` FOREIGN KEY (`E_T_Id`) REFERENCES `Teacher` (`T_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计分-教师中间表';

-- ----------------------------
-- Table structure for Student
-- ----------------------------
DROP TABLE IF EXISTS `Student`;
CREATE TABLE `Student` (
  `S_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生表ID',
  `S_No` varchar(20) NOT NULL DEFAULT '' COMMENT '学生学号',
  `S_Name` varchar(20) NOT NULL DEFAULT '' COMMENT '学生姓名',
  `S_Pass` varchar(35) NOT NULL DEFAULT '' COMMENT '学生密码',
  `S_Sex` varchar(2) NOT NULL DEFAULT '0' COMMENT '学生性别',
  `S_Type` varchar(2) NOT NULL DEFAULT '0' COMMENT '学生类型（1在校2退学3休学）',
  `Class_Id` int(11) NOT NULL DEFAULT '0' COMMENT '班级id',
  PRIMARY KEY (`S_Id`),
  UNIQUE KEY `unique_sno` (`S_No`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COMMENT='学生表';

-- ----------------------------
-- Table structure for Teacher
-- ----------------------------
DROP TABLE IF EXISTS `Teacher`;
CREATE TABLE `Teacher` (
  `T_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '教师表ID',
  `T_No` varchar(20) NOT NULL DEFAULT '' COMMENT '教师工号',
  `T_Name` varchar(20) NOT NULL DEFAULT '' COMMENT '教师姓名',
  `T_Pass` varchar(35) NOT NULL DEFAULT '' COMMENT '教师密码建议md5加密',
  `T_Sex` varchar(2) NOT NULL DEFAULT '0' COMMENT '教师性别（1男0女）',
  `T_Status` varchar(2) NOT NULL DEFAULT '0' COMMENT '教师状态（1在职0离职）',
  `D_Id` int(11) DEFAULT '0' COMMENT '部门id 关联系部id',
  PRIMARY KEY (`T_Id`),
  UNIQUE KEY `unique_TNo` (`T_No`) USING BTREE,
  KEY `Teacher_D_id_PK` (`D_Id`),
  CONSTRAINT `Teacher_D_id_PK` FOREIGN KEY (`D_Id`) REFERENCES `Department` (`D_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='教师表';

-- ----------------------------
-- Table structure for Teacher_Course
-- ----------------------------
DROP TABLE IF EXISTS `Teacher_Course`;
CREATE TABLE `Teacher_Course` (
  `TC_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `T_Id` int(11) NOT NULL DEFAULT '0' COMMENT '教师id',
  `C_Id` int(11) NOT NULL DEFAULT '0' COMMENT '课程id',
  PRIMARY KEY (`TC_Id`),
  KEY `FK_T_Id` (`T_Id`),
  KEY `FK_C_Id` (`C_Id`),
  CONSTRAINT `FK_C_Id` FOREIGN KEY (`C_Id`) REFERENCES `Course` (`C_Id`),
  CONSTRAINT `FK_T_Id` FOREIGN KEY (`T_Id`) REFERENCES `Teacher` (`T_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='教师-课程中间表';
