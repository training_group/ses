package cn.group.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Course;
import cn.group.service.CourseService;
import cn.group.service.Impl.CourseVoServiceImpl;
import cn.group.utils.AjaxResult;
import cn.group.vo.CourseVo;

/**
 * 课程(Course)Controller
 * @author Shinelon
 *
 */
@RestController
@RequestMapping("/course")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseVoServiceImpl CourseVoService;

    /**
     * 添加课程
     * @param course
     * @return AjaxResult
     */
    @PostMapping("/insert")
    public AjaxResult InsertCourse(@RequestBody Course course) {
        int insertCourse = courseService.InsertCourse(course);
        if (insertCourse > 0)
            return AjaxResult.Success("新增课程成功！");
        else
            return AjaxResult.fail("新增课程失败！");
    }

    /**
     * 查询课程
     * @param course
     * @return AjaxResult
     */
    @PostMapping("/all")
    public AjaxResult SelectCourse(@RequestBody Course course) {
        List<Course> selectCourse = courseService.SelectCourse(course);
        return AjaxResult.Success(null, selectCourse);
    }

    /**
     * 修改课程信息
     * @param course
     * @return AjaxResult
     */
    @PutMapping
    public AjaxResult UpdateCourse(Course course) {
        AjaxResult ajaxResult = new AjaxResult();
        String msg = "";
        int value = 0;
        if (course.getcId() != null) {
            if (courseService.UpdateCourse(course) > 0) {
                value = HttpStatus.OK.value();
                msg = "修改成功";
            } else {
                value = HttpStatus.BAD_REQUEST.value();
                msg = "修改失败";
            }
        } else {
            value = HttpStatus.BAD_REQUEST.value();
            msg = "数据异常,缺少关键数据";
        }
        ajaxResult.setStatus(value);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    /**
     * 删除课程
     * @param cIds
     * @return AjaxResult
     */
    @DeleteMapping
    public AjaxResult DeleteCourse(@RequestBody List<String> cIds) {
        AjaxResult ajaxResult = new AjaxResult();
        String msg = "";
        int succ = 0;// 计数
        int fail = 0;// 计数
        int value = 0;
        for (String cId : cIds) {
            Course course = new Course();
            course.setcId(Integer.valueOf(cId));
            if (courseService.DeleteCourse(course) > 0) {
                succ++;
            } else {
                fail++;
            }
        }
        msg = succ + "条数据成功，" + fail + "条数据失败";
        ajaxResult.setStatus(value);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    /**
     * 排课
     * @param courseVo
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/plan")
    public AjaxResult insertClassCourse(@RequestBody CourseVo courseVo) {
        int insert = CourseVoService.insert(courseVo);
        if (insert > 0) {
            return AjaxResult.Success("排课操作成功！");
        } else if (insert == -1) {
            return AjaxResult.fail("课次已存在！");
        } else
            return AjaxResult.fail("排课失败,稍后再试！");
    }

    /**
     * 查询排课列表
     * @param courseVo
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/vo")
    public AjaxResult selectCourseVo(@RequestBody CourseVo courseVo) {
        List<CourseVo> selectCourseVo = courseService.selectCourseVo(courseVo);
        return AjaxResult.Success(null, selectCourseVo);
    }

    /**
     * 删除排课
     * @param courseVo
     * @return
     * AjaxResult
     *
     */
    @DeleteMapping("/vo")
    public AjaxResult deleteCourseVo(@RequestBody CourseVo courseVo) {
        int deleteCourseVo = CourseVoService.deleteCourseVo(courseVo);
        if (deleteCourseVo > 0) {
            return AjaxResult.Success("删除成功！");
        }
        return AjaxResult.fail("删除失败！");
    }

}
