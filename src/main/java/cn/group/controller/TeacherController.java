package cn.group.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import cn.group.entity.Department;
import cn.group.entity.Teacher;
import cn.group.service.DepartmentService;
import cn.group.service.TeacherService;
import cn.group.utils.AjaxResult;
import cn.group.utils.Cnst;
import cn.group.utils.Mytool;
import cn.group.vo.TeacherEvaluateVo;
import cn.group.vo.TeacherInfo;

/**
 * 教师（Teacher）Controller
 * @author Shinelon
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@SessionAttributes(value = { Cnst.LOGINUSER })
@RequestMapping("/teacher")
public class TeacherController {

    private static final Logger log = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    TeacherService teacherService;
    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/add")
    public AjaxResult insert(@RequestBody Teacher teacher) {
        int insert = teacherService.insert(teacher);
        if (insert > 0) {
            return AjaxResult.Success("添加教师成功");
        } else {
            return AjaxResult.fail("添加失败");
        }
    }

    /**
     * 教师登录
     * @param teacher
     * @return AjaxResult
     */
    @PostMapping("/login")
    public AjaxResult Login(Teacher teacher, String code, HttpSession session) {
        AjaxResult ajaxResult = new AjaxResult();
        String vcode = (String) session.getAttribute(session.getId());
        if (StringUtils.isNotBlank(code) && StringUtils.equalsIgnoreCase(code, vcode)) {
            String no = teacher.gettNo();
            log.info("工号:{}请求登录", no);
            List<TeacherInfo> selectTeacher = teacherService.SelectTeacher(teacher);
            if (CollectionUtils.isEmpty(selectTeacher)) { // 判断教师是否存在
                log.info("工号：{}账户不存在", no);
                ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
                ajaxResult.setMsg("该教师不存在");
            } else if (StringUtils.equals(Mytool.getMD5(teacher.gettPass()),
                    selectTeacher.get(0).getTeacher().gettPass())) { // 判断密码是否正确
                log.info("工号：{}账户认证登录成功", no);
                session.removeAttribute(Cnst.LOGINUSER);
                session.setAttribute(Cnst.LOGINUSER, selectTeacher.get(0));
                ajaxResult.setStatus(HttpStatus.OK.value());
                ajaxResult.setData(selectTeacher.get(0));
            } else {
                log.info("工号：{}账户登录密码错误", no);
                ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
                ajaxResult.setMsg("密码错误");
            }
        } else {
            ajaxResult.setStatus(-1);
            ajaxResult.setMsg("验证码错误！");
        }
        return ajaxResult;

    }

    /**
     * 重置密码
     * @param tNos
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/restpw")
    public AjaxResult resetPassword(@RequestBody List<String> tNos) {
        int count = teacherService.resetPassword(tNos);
        return new AjaxResult(HttpStatus.OK.value(), count + "名教师重置密码成功！", null);
    }

    @PutMapping("/changepw")
    public AjaxResult changePassword(String oldPw, String newPw, String newPw2, HttpSession session) {
        TeacherInfo teacher = (TeacherInfo) session.getAttribute(Cnst.LOGINUSER);
        log.info("教师:{}执行操作修改密码操作。", teacher.getTeacher().gettNo());
        Teacher t = new Teacher();
        t.settId(teacher.getTeacher().gettId());
        TeacherInfo teacherInfo = teacherService.SelectTeacher(t).get(0);
        if (StringUtils.isBlank(oldPw)
                || !StringUtils.equals(Mytool.getMD5(oldPw), teacherInfo.getTeacher().gettPass())) {
            log.info("原始密码错误！");
            return AjaxResult.fail("原始密码错误！");
        }
        if (StringUtils.isBlank(newPw) || !StringUtils.equals(newPw, newPw2)) {
            log.info("新密码不一致");
            return AjaxResult.fail("新密码不一致");
        }
        t.settPass(newPw);
        int update = teacherService.UpdateTeacher(t);
        if (update > 0) {
            log.info("sessionId:{}教师:{}修改密码成功！", session.getId(), teacher.getTeacher().gettNo());
            // 清空session,重新登录
            session.removeAttribute(Cnst.LOGINUSER);
            return AjaxResult.Success("修改密码成功！需重新登录。");
        }
        return AjaxResult.fail("修改失败，稍后再试！");
    }

    /**
     * 修改教师信息
     * @param teachers
     * @return AjaxResult
     */
    @PutMapping("/update")
    public AjaxResult UpdateTeacher(@RequestBody Teacher teacher) {
        teacherService.UpdateTeacher(teacher);
        return AjaxResult.Success("修改成功！");
    }

    /**
     * 查询教师列表
     * @param teacher
     * @return
     * AjaxResult
     *
     */
    @PostMapping
    public AjaxResult select(@RequestBody Teacher teacher) {
        List<TeacherInfo> Teacherinfos = teacherService.SelectTeacher(teacher);
        return AjaxResult.Success(null, Teacherinfos);
    }

    /**
     * 批量修改教师状态
     * @param ids
     * @param type
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/changeType/{type}")
    public AjaxResult changeType(@RequestBody List<Integer> tIds, @PathVariable("type") final String type) {
        int changeType = teacherService.changeType(tIds, type);
        return new AjaxResult(HttpStatus.OK.value(), changeType + "条记录,修改类型成功！", null);
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * AjaxResult
     *
     */
    @DeleteMapping
    public AjaxResult delete(@RequestBody List<Integer> ids) {
        int delete = teacherService.delete(ids);
        return new AjaxResult(HttpStatus.OK.value(), "成功删除" + delete + "条记录！", null);
    }

    /**
     * csv文件导入
     * @param importfile
     * @return
     * AjaxResult
     *
     */

    private AjaxResult importCSV(InputStream inputStream) {
        int count = 0;
        log.info("csv导入教师开始...");
        AjaxResult ajaxResult = new AjaxResult();
        try {
            List<String> readLines = IOUtils.readLines(inputStream);
            // 空文件
            if (CollectionUtils.isEmpty(readLines)) {
                ajaxResult.setStatus(1);
                ajaxResult.setMsg("导入失败：空文件");
            } else {
                List<Teacher> teachers = new ArrayList<>(100);
                Teacher teacher = null;
                // 去除标题行
                readLines.remove(0);
                for (String line : readLines) {
                    String properties[] = StringUtils.split(StringUtils.trim(line), ",");
                    if (ArrayUtils.isNotEmpty(properties)) {
                        teacher = new Teacher();
                        teacher.settNo(StringUtils.trim(properties[0]));
                        teacher.settName(StringUtils.trim(properties[1]));
                        teacher.settPass(Mytool.getMD5(teacher.gettNo()));
                        // 性别
                        switch (StringUtils.trim(properties[2])) {
                        case "男":
                        case Cnst.MALE:
                        case "男性":
                            teacher.settSex(Cnst.MALE);
                            break;

                        default:
                            teacher.settSex(Cnst.FEMALE);
                            break;
                        }
                        // 状态
                        switch (StringUtils.trim(properties[3])) {
                        case "在职":
                        case Cnst.TEACHER_STATUS_ONJOB:
                            teacher.settStatus(Cnst.TEACHER_STATUS_ONJOB);
                            break;
                        default:
                            teacher.settStatus(Cnst.TEACHER_STATUS_QUIT);
                            break;
                        }
                        // 教师院系
                        Department department = departmentService.selectByDname(StringUtils.trim(properties[4]));
                        if (department != null) {
                            teacher.setdId(department.getdId());
                        }
                        teachers.add(teacher);

                        if (teachers.size() % 100 == 0) {
                            // 100条插入一次
                            count += teacherService.insertBatch(teachers);
                            teachers.clear();
                        }
                    }

                }
                if (!CollectionUtils.isEmpty(teachers)) {
                    count += teacherService.insertBatch(teachers);
                }
                ajaxResult.setStatus(HttpStatus.OK.value());
                ajaxResult.setMsg("成功导入" + count + "条记录");
                log.info("新增{}条教师数据!", count);
            }
        } catch (IOException e) {
            ajaxResult.setStatus(2);
            ajaxResult.setMsg("文件读取失败，请校验csv文件格式");
            e.printStackTrace();
        }
        return ajaxResult;
    }

    @PostMapping("/import")
    public AjaxResult importStudent(@RequestParam("file") final MultipartFile importfile) {
        int count = 0;
        InputStream inputStream = null;
        // 文件名
        String fileName = importfile.getOriginalFilename().toLowerCase();
        // 文件格式
        final String type = fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            inputStream = importfile.getInputStream();
            switch (type) {
            case "csv":
                return importCSV(inputStream);
            case "xls":// 2003
                count = importExcel(new HSSFWorkbook(inputStream));
                break;
            case "xlsx":// 2007
                count = importExcel(new XSSFWorkbook(inputStream));
                break;
            default:
                log.info("导入的文件格式错误，允许的格式：csv、xls、xlsx");
                return AjaxResult.fail("导入的文件格式错误，允许的格式：csv、xls、xlsx");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return AjaxResult.Success("成功导入" + count + "条数据!");
    }

    /**
     * excel格式导入
     * @param wb
     * int 成功条数
     *
     */
    private int importExcel(Workbook wb) {
        int count = 0;
        log.info("excel导入教师开始...");
        List<Teacher> teachers = new ArrayList<>(100);
        Teacher teacher = null;
        // 获取sheet数量
        int numberOfSheets = wb.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = wb.getSheetAt(i);
            // 行数
            int RowNum = sheet.getPhysicalNumberOfRows();
            // 跳过标题行
            for (int j = 1; j < RowNum; j++) {
                teacher = new Teacher();
                Row row = sheet.getRow(j);
                // 得到单元格数
                // int physicalNumberOfCells = row.getPhysicalNumberOfCells();
                String tNo = row.getCell(0).getStringCellValue();
                teacher.settNo(StringUtils.trim(tNo));
                String tName = row.getCell(1).getStringCellValue();
                teacher.settName(StringUtils.trim(tName));
                teacher.settPass(Mytool.getMD5(teacher.gettNo()));
                String tSex = row.getCell(2).getStringCellValue();
                // 性别
                switch (StringUtils.trim(tSex)) {
                case "男":
                case Cnst.MALE:
                case "男性":
                    teacher.settSex(Cnst.MALE);
                    break;

                default:
                    teacher.settSex(Cnst.FEMALE);
                    break;
                }
                String tStatus = row.getCell(3).getStringCellValue();
                // 状态
                switch (StringUtils.trim(tStatus)) {
                case "在职":
                case Cnst.TEACHER_STATUS_ONJOB:
                    teacher.settStatus(Cnst.TEACHER_STATUS_ONJOB);
                    break;
                default:
                    teacher.settStatus(Cnst.TEACHER_STATUS_QUIT);
                    break;
                }
                // 教师院系
                String dName = row.getCell(4).getStringCellValue();
                Department department = departmentService.selectByDname(StringUtils.trim(dName));
                if (department != null) {
                    teacher.setdId(department.getdId());
                }
                log.info("读取第{}张sheet,第{}行数据:{}", i, j, teacher.gettNo() + teacher.gettName());
                teachers.add(teacher);
                if (teachers.size() % 100 == 0) {
                    // 100条插入一次
                    count += teacherService.insertBatch(teachers);
                    teachers.clear();
                }
            }

        }
        if (!CollectionUtils.isEmpty(teachers)) {
            count += teacherService.insertBatch(teachers);
        }
        log.info("新增{}条教师数据!", count);
        return count;
    }

    @PostMapping("/exit")
    public void exit(HttpSession session) {
        TeacherInfo teacherInfo = (TeacherInfo) session.getAttribute(Cnst.LOGINUSER);
        log.info("session{},教师:{}退出登录！", session.getId(), teacherInfo.getTeacher().gettName());
        session.invalidate();
    }

    /**
     * 查询评教列表
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/eval/{tId}")
    public AjaxResult selectEvaluate(@PathVariable("tId") Integer tId) {
        Teacher t = new Teacher();
        t.settId(tId);
        List<TeacherInfo> selectTeacher = teacherService.SelectTeacher(t);
        if (CollectionUtils.isEmpty(selectTeacher)) {
            log.info("session中用户信息失效！");
            throw new NullPointerException("登录信息失效！");
        }
        List<TeacherEvaluateVo> selectEvaluate = teacherService.selectEvaluate(selectTeacher.get(0).getTeacher());
        return AjaxResult.Success(null, selectEvaluate);
    }
}
