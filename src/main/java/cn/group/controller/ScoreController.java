package cn.group.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Score;
import cn.group.entity.ScoreTea;
import cn.group.entity.Scorestu;
import cn.group.service.ScoreService;
import cn.group.service.ScoreTeaService;
import cn.group.service.ScorestuService;
import cn.group.utils.AjaxResult;
import cn.group.vo.CourseVo;
import cn.group.vo.ScoreStuVo;
import cn.group.vo.ScoreTeaVo;
import cn.group.vo.ScoreTeacherStudentVo;
import cn.group.vo.ScoreTeacherTeacherVo;
import cn.group.vo.StudentEvaluateDetail;

/**
 * 计分（score）Controller
 * @author Shinelon
 * @createDate 2018/09/12
 */
@RestController
@RequestMapping("/score")
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    @Autowired
    private ScorestuService scorestuservice;

    @Autowired
    private ScoreTeaService scoreteaservice;

    /**
     * 查询学生计分信息
     * 
     * @return AjaxResult
     */
    @PostMapping("/stdScore")
    public AjaxResult SelectStudentScoreVo(@RequestBody CourseVo courseVo) {
        List<ScoreStuVo> selectScoreVo = scoreService.selectScoreVo(courseVo);
        return AjaxResult.Success(null, selectScoreVo);
    }

    /**
     * 查询教师计分信息
     * @param dptId 院系id
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/teaScore/{dptId}")
    public AjaxResult SelectTeacherScoreVo(@PathVariable("dptId") Integer dptId) {
        List<ScoreTeaVo> selectScoreVo = scoreService.selectScoreVo(dptId);
        return AjaxResult.Success(null, selectScoreVo);
    }

    /**
     * 添加学生计分数据
     * @param score
     * @return AjaxResult
     */
    @PostMapping("/std")
    public AjaxResult InsertScore(@RequestBody ScoreTeacherStudentVo stsVo) {
        Double scoreNum = stsVo.getScore();
        if (scoreNum == null) {
            throw new IllegalArgumentException("分数不能为空");
        }
        Integer tId = stsVo.gettId();
        if (tId == null) {
            throw new IllegalArgumentException("教师id不能为空");
        }
        Integer sId = stsVo.getsId();
        if (sId == null) {
            throw new IllegalArgumentException("学生id不能为空");
        }
        Scorestu scorestu = new Scorestu();
        scorestu.settId(tId);
        scorestu.setsId(sId);
        if (scorestuservice.SelectScorestu(scorestu).size() <= 0) {// 判断是否已经存在
            Score score = new Score();
            score.setScoreNum(scoreNum);
            score.settId(tId);
            if (scoreService.InsertScore(score) > 0) {// 向score表添加数据
                // 获得Score id
                Integer scoreId = score.getScoreId();
                scorestu.setSsId(scoreId);// 获得Score id
                if (scorestuservice.InsertScorestu(scorestu) > 0) {// 向score_stu表添加数据
                    return AjaxResult.Success("评教成功！");
                } else {
                    return AjaxResult.fail("评教失败");
                }
            } else {
                return AjaxResult.fail("评教失败");
            }
        } else {
            return AjaxResult.Success("不能重复评教");
        }
    }

    /**
     * 添加教师计分数据
     * @param score
     * @return AjaxResult
     */
    @PostMapping("/tea")
    public AjaxResult InsertScore(@RequestBody ScoreTeacherTeacherVo teaVo) {
        Double scoreNum = teaVo.getScore();
        if (scoreNum == null) {
            throw new IllegalArgumentException("分数不能为空");
        }
        Integer tId = teaVo.gettId();// 被评教教师id
        if (tId == null) {
            throw new IllegalArgumentException("被评教教师id不能为空");
        }
        Integer eTId = teaVo.geteTId();// 教师id
        if (eTId == null) {
            throw new IllegalArgumentException("教师id不能为空");
        }
        ScoreTea scoreTea = new ScoreTea();
        scoreTea.seteTId(eTId);
        scoreTea.settId(tId);
        if (scoreteaservice.SelectScoreTea(scoreTea).size() <= 0) {
            Score score = new Score();
            score.setScoreNum(scoreNum);
            score.settId(tId);
            if (scoreService.InsertScore(score) > 0) {// 向score表添加数据
                // 获得Score id
                Integer scoreId = score.getScoreId();
                scoreTea.setStId(scoreId);// 获得Score id
                if (scoreteaservice.insertScoreTea(scoreTea) > 0) {// 向score_tea表添加数据
                    return AjaxResult.Success("评教成功！");
                } else {
                    return AjaxResult.fail("评教失败");
                }
            } else {
                return AjaxResult.fail("评教失败");
            }
        } else {
            return AjaxResult.Success("不能重复评教");
        }
    }

    /**
     * 学生评价明细，已评未评
     * @param score
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/std/{tId}/{classId}")
    public AjaxResult studentEvaluateDetail(@PathVariable("tId") Integer tId, @PathVariable("classId") String classId) {
        List<StudentEvaluateDetail> studentEvaluateDetail = scoreService.studentEvaluateDetail(tId, classId);
        return AjaxResult.Success(null, studentEvaluateDetail);
    }
}
