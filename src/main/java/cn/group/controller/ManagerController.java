package cn.group.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import cn.group.entity.Manager;
import cn.group.service.ManagerService;
import cn.group.utils.AjaxResult;
import cn.group.utils.Cnst;
import cn.group.utils.Mytool;

/**
 * 管理员(Manager)Controller
 * @author Shinelon
 *
 */
@RestController
@RequestMapping("/manager")
@SessionAttributes(value = { Cnst.LOGINUSER })
public class ManagerController {

    private static final Logger log = LoggerFactory.getLogger(ManagerController.class);

    @Autowired
    ManagerService managerService;

    /**
     * 管理员登录Controller
     * @param manager
     * @return AjaxResult
     */
    @PostMapping("/login")
    public AjaxResult Login(Manager manager, String code, HttpSession session) {
        AjaxResult ajaxResult = new AjaxResult();
        String vcode = (String) session.getAttribute(session.getId());
        log.info("管理员:{}请求登录！", manager.getmAccount());
        if (StringUtils.isNotBlank(code) && StringUtils.equalsIgnoreCase(code, vcode)) {
            List<Manager> selectManager = managerService.SelectManager(manager);
            if (CollectionUtils.isEmpty(selectManager)) { // 判断管理员是否存在
                ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
                log.info("管理员:{}账户不存在！", manager.getmAccount());
                ajaxResult.setMsg("账户不存在");
            } else if (StringUtils.equals(Mytool.getMD5(manager.getmPass()), selectManager.get(0).getmPass())) { // 管理员存在，验证密码是否正确，密码正确
                log.info("管理员:{}登录验证成功！", manager.getmAccount());
                session.setAttribute(Cnst.LOGINUSER, selectManager.get(0));
                ajaxResult.setStatus(HttpStatus.OK.value());
                ajaxResult.setData(selectManager.get(0));
            } else { // 密码错误
                ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
                log.info("管理员:{}登录密码错误！", manager.getmAccount());
                ajaxResult.setMsg("密码错误");
            }
        } else {
            ajaxResult.setStatus(-1);
            log.info("管理员:{}登录验证码错误！", manager.getmAccount());
            ajaxResult.setMsg("验证码错误！");
        }
        return ajaxResult;
    }

    /**
     * 修改密码
     * @param oldPw
     * @param newPw
     * @param newPw2
     * @param session
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/changepw")
    public AjaxResult changePassword(String oldPw, String newPw, String newPw2, HttpSession session) {
        Manager manager = (Manager) session.getAttribute(Cnst.LOGINUSER);
        log.info("管理员:{}执行操作修改密码操作。", manager.getmAccount());
        Manager m = new Manager();
        m.setmId(manager.getmId());
        List<Manager> selectManager = managerService.SelectManager(m);
        log.info(Mytool.getMD5(oldPw)+"---"+selectManager.get(0).getmPass());
        if (StringUtils.isBlank(oldPw) || !StringUtils.equals(Mytool.getMD5(oldPw), selectManager.get(0).getmPass())) {
            log.info("原始密码错误！");
            return AjaxResult.fail("原始密码错误！");
        }
        if (StringUtils.isBlank(newPw) || !StringUtils.equals(newPw, newPw2)) {
            log.info("新密码不一致");
            return AjaxResult.fail("新密码不一致");
        }
        m.setmPass(newPw);
        int update = managerService.updateByPrimaryKey(m);
        if (update > 0) {
            log.info("sessionId:{}管理员:{}修改密码成功！", session.getId(), manager.getmAccount());
            // 清空session,重新登录
            session.removeAttribute(Cnst.LOGINUSER);
            return AjaxResult.Success("修改密码成功！需重新登录。");
        }
        return AjaxResult.fail("修改失败，稍后再试！");
    }

}
