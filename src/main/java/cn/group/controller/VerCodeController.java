package cn.group.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.utils.AjaxResult;
import cn.group.utils.VerifyCodeUtils;

/**
 * 验证码
 * 
 * @author Shinelon
 * @createDate 2018/09/17
 */
@RestController
@RequestMapping("/vercode")
public class VerCodeController {

    private static final Logger log = LoggerFactory.getLogger(VerCodeController.class);

    /**
     * 生成验证码
     * @param request
     * @param response
     * @throws IOException 
     */
    @GetMapping("/code")
    public void getVerify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");

        // 生成随机字串
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        // 存入会话session
        session.setAttribute(session.getId(), verifyCode);
        log.info("sessionId:{}获取登录验证码:{}", session.getId(), verifyCode);
        // 生成图片
        VerifyCodeUtils.outputImage(90, 35, response.getOutputStream(), verifyCode);
    }

    /**
     * 验证验证码
     * @param code
     * @param session
     * @return AjaxResult
     */
    @RequestMapping(value = "/check")
    public AjaxResult checkVerify(String code, HttpSession session) {
        AjaxResult ajaxResult = new AjaxResult();
        String msg = "";
        int status = 0;
        try {
            // 从session中获取随机数
            String random = (String) session.getAttribute(session.getId());
            if (random == null) {
                msg = code + "验证码生成出错,请刷新";
                status = HttpStatus.BAD_REQUEST.value();
            }
            if (random.equals(code)) {
                msg = code + "验证码正确";
                status = HttpStatus.OK.value();
            } else {
                msg = code + "验证码错误";
                status = HttpStatus.BAD_REQUEST.value();
            }
        } catch (Exception e) {
            msg = code + "数据异常";
            status = HttpStatus.BAD_REQUEST.value();
            System.err.println(e);
        }
        ajaxResult.setStatus(status);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }
}
