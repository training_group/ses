package cn.group.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Scorestu;
import cn.group.service.ScorestuService;
import cn.group.utils.AjaxResult;

/**
 * 
 * @author Shinelon
 * @createDate 2018/09/13
 */
@RestController
@RequestMapping("/scorestu")
public class ScorestuController {

	@Autowired
	ScorestuService scorestuservice;
	
	/**
	 * 查询
	 * @param scorestu
	 * @return
	 */
	@PostMapping("/select")
	public AjaxResult SelectScorestu(Scorestu scorestu){
		AjaxResult ajaxResult = new AjaxResult();
		String msg="";
		int status=0;
		Object data = new Object();
		if(scorestu!=null){
			data=scorestuservice.SelectScorestu(scorestu);
			status=HttpStatus.OK.value();
		}else{
			status=HttpStatus.BAD_REQUEST.value();
		}
		ajaxResult.setStatus(status);
		ajaxResult.setMsg(msg);
		ajaxResult.setData(data);
		return ajaxResult;
	}
	
	/**
	 * 添加
	 * @param scorestu
	 * @return
	 */
	@PostMapping("/insert")
	public AjaxResult InsertScorestu(Scorestu scorestu){
		AjaxResult ajaxResult = new AjaxResult();
		String msg="";
		int status=0;
		if(scorestu!=null){
			if(scorestuservice.InsertScorestu(scorestu)>0){
				status=HttpStatus.OK.value();
				msg="添加成功";
			}else{
				status=HttpStatus.BAD_REQUEST.value();
				msg="添加失败";
			}
		}else{
			status=HttpStatus.BAD_REQUEST.value();
			msg="数据异常";
		}
		ajaxResult.setStatus(status);
		ajaxResult.setMsg(msg);
		return ajaxResult;
	}
	
	@PostMapping("/update")
	public AjaxResult UpdateScorestu(Scorestu scorestu){
		AjaxResult ajaxResult = new AjaxResult();
		String msg="";
		int status=0;
		if(scorestu!=null&&scorestu.getSsId()!=null){
			if(scorestuservice.UpdateScorestu(scorestu)>0){
				status=HttpStatus.OK.value();
				msg="修改成功";
			}else{
				status=HttpStatus.BAD_REQUEST.value();
				msg="修改失败";
			}
		}else{
			status=HttpStatus.BAD_REQUEST.value();
			msg="数据异常";
		}
		ajaxResult.setStatus(status);
		ajaxResult.setMsg(msg);
		return ajaxResult;
	}
	
	@PostMapping("/delete")
	public AjaxResult DeleteScorestu(Scorestu scorestu){
		AjaxResult ajaxResult=new AjaxResult();
		String msg="";
		int status=0;
		if(scorestu.getSsId()!=null){
			if(scorestuservice.DeleteScorestu(scorestu)>0){
				status=HttpStatus.OK.value();
				msg="删除成功";
			}else{
				status=HttpStatus.BAD_REQUEST.value();
				msg="删除失败";
			}
		}else{
			status=HttpStatus.BAD_REQUEST.value();
			msg="数据异常";
		}
		ajaxResult.setStatus(status);
		ajaxResult.setMsg(msg);
		return ajaxResult;
	}
}
