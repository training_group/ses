package cn.group.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Class;
import cn.group.service.ClassService;
import cn.group.utils.AjaxResult;

/**
 * 班级controller
 * @author fzy
 * 2018年9月10日 下午4:44:32
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/class")
public class ClassController {

    private static final Logger log = LoggerFactory.getLogger(ClassController.class);

    @Autowired
    private ClassService classService;

    /**
     * 增加班级
     * @param c
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/add")
    public AjaxResult insert(@RequestBody Class c) {
        int add = classService.add(c);
        if (add > 0) {
            return AjaxResult.Success("添加班级成功");
        } else {
            return AjaxResult.fail("增加失败！");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * AjaxResult
     *
     */
    @DeleteMapping
    public AjaxResult delete(@RequestBody List<Integer> ids) {
        int count = classService.delete(ids);
        return AjaxResult.Success("成功删除" + count + "条记录");
    }

    /**
     * 查询所有
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/all")
    public AjaxResult selectAll(@RequestBody Class c) {
        List<Class> selectAll = classService.select(c);
        return AjaxResult.Success(null, selectAll);
    }
}
