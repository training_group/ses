package cn.group.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import cn.group.entity.Student;
import cn.group.service.StudentService;
import cn.group.utils.AjaxResult;
import cn.group.utils.Cnst;
import cn.group.utils.Mytool;
import cn.group.vo.StudentEvaluateVo;

@RestController
@RequestMapping("/student")
@CrossOrigin(origins = "*", maxAge = 3600)
@SessionAttributes(value = { Cnst.LOGINUSER })
public class StudentController {

    private static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    /**
     * 登录
     * @param student
     * @param session
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/login")
    public AjaxResult login(Student student, String code, HttpSession session) {
        AjaxResult AjaxResult = new AjaxResult();
        String vcode = (String) session.getAttribute(session.getId());
        if (StringUtils.isNotBlank(code) && StringUtils.equalsIgnoreCase(code, vcode)) {
            String no = student.getsNo();
            log.info("学号:{}请求登录", no);
            List<Student> students = studentService.login(student);
            if (CollectionUtils.isEmpty(students)) {
                log.info("学号：{}账户不存在", no);
                AjaxResult.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
                AjaxResult.setMsg("账户不存在");
            } else if (!StringUtils.equals(Mytool.getMD5(student.getsPass()), students.get(0).getsPass())) {
                log.info("学号：{}账户登录密码错误", no);
                AjaxResult.setStatus(HttpStatus.UNAUTHORIZED.value());
                AjaxResult.setMsg("密码错误");
            } else {
                log.info("学号：{}账户认证登录成功", no);
                AjaxResult.setStatus(HttpStatus.OK.value());
                AjaxResult.setMsg("登录验证成功");
                session.removeAttribute(Cnst.LOGINUSER);
                session.setAttribute(Cnst.LOGINUSER, students.get(0));
                log.info("sessionId:{}密码:{}", session.getId(), students.get(0).getsPass());
                AjaxResult.setData(students.get(0));
            }
        } else {
            AjaxResult.setStatus(-1);
            AjaxResult.setMsg("验证码错误！");
        }
        return AjaxResult;
    }

    /**
     * 添加学生
     * @param student
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/add")
    public AjaxResult addStudent(@RequestBody Student student) {
        AjaxResult ajaxResult = new AjaxResult();
        int insert = studentService.insert(student);
        if (insert > 0) {
            ajaxResult.setStatus(HttpStatus.OK.value());
            ajaxResult.setMsg("添加学生成功！");
        } else if (insert == -1) {
            ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
            ajaxResult.setMsg("该学生已存在！");
        } else {
            ajaxResult.setStatus(HttpStatus.BAD_REQUEST.value());
            ajaxResult.setMsg("添加学生失败！稍后再试");
        }
        return ajaxResult;
    }

    /**
     * csv文件导入
     * @param importfile
     * @return
     * AjaxResult
     *
     */

    private AjaxResult importCSV(InputStream inputStream) {
        int count = 0;
        log.info("csv导入学生开始...");
        AjaxResult ajaxResult = new AjaxResult();
        try {
            List<String> readLines = IOUtils.readLines(inputStream);
            // 空文件
            if (CollectionUtils.isEmpty(readLines)) {
                ajaxResult.setStatus(1);
                ajaxResult.setMsg("导入失败：空文件");
            } else {
                List<Student> Students = new ArrayList<>(100);
                Student student = null;
                // 去除标题行
                readLines.remove(0);
                for (String line : readLines) {
                    String properties[] = StringUtils.split(StringUtils.trim(line), ",");
                    if (ArrayUtils.isNotEmpty(properties)) {
                        student = new Student();
                        student.setsNo(StringUtils.trim(properties[0]));
                        student.setsName(StringUtils.trim(properties[1]));
                        student.setsPass(Mytool.getMD5(student.getsNo()));
                        // 性别
                        switch (StringUtils.trim(properties[2])) {
                        case "男":
                        case Cnst.MALE:
                        case "男性":
                            student.setsSex(Cnst.MALE);
                            break;

                        default:
                            student.setsSex(Cnst.FEMALE);
                            break;
                        }
                        // 类型
                        switch (StringUtils.trim(properties[3])) {
                        case "在校":
                        case Cnst.STUDENT_TYPE_ATSCHOOL:
                            student.setsType(Cnst.STUDENT_TYPE_ATSCHOOL);
                            break;
                        case "退学":
                        case Cnst.STUDENT_TYPE_OUTSCHOOL:
                            student.setsType(Cnst.STUDENT_TYPE_OUTSCHOOL);
                            break;

                        default:
                            student.setsType(Cnst.STUDENT_TYPE_LEAVESCHOOL);
                            break;
                        }
                        student.setClassId(Integer.parseInt(StringUtils.trim(properties[4])));
                        Students.add(student);

                        if (Students.size() % 100 == 0) {
                            // 100条插入一次
                            count += studentService.insertBatch(Students);
                            Students.clear();
                        }
                    }

                }
                if (!CollectionUtils.isEmpty(Students)) {
                    count += studentService.insertBatch(Students);
                }
                ajaxResult.setStatus(HttpStatus.OK.value());
                ajaxResult.setMsg("成功导入" + count + "条记录");
                log.info("新增{}条学生数据!", count);
            }
        } catch (IOException e) {
            ajaxResult.setStatus(2);
            ajaxResult.setMsg("文件读取失败，请校验csv文件格式");
            e.printStackTrace();
        }
        return ajaxResult;
    }

    @PostMapping("/import")
    public AjaxResult importStudent(@RequestParam("file") final MultipartFile importfile) {
        int count = 0;
        InputStream inputStream = null;
        // 文件名
        String fileName = importfile.getOriginalFilename().toLowerCase();
        // 文件格式
        final String type = fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            inputStream = importfile.getInputStream();
            switch (type) {
            case "csv":
                return importCSV(inputStream);
            case "xls":// 2003
                count = importExcel(new HSSFWorkbook(inputStream));
                break;
            case "xlsx":// 2007
                count = importExcel(new XSSFWorkbook(inputStream));
                break;
            default:
                log.info("导入的文件格式错误，允许的格式：csv、xls、xlsx");
                return AjaxResult.fail("导入的文件格式错误，允许的格式：csv、xls、xlsx");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return AjaxResult.Success("成功导入" + count + "条数据!");
    }

    /**
     * excel格式导入
     * @param wb
     * int 成功条数
     *
     */
    private int importExcel(Workbook wb) {
        int count = 0;
        log.info("excel导入学生开始...");
        List<Student> Students = new ArrayList<>(100);
        Student student = null;
        // 获取sheet数量
        int numberOfSheets = wb.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = wb.getSheetAt(i);
            // 行数
            int RowNum = sheet.getPhysicalNumberOfRows();
            // 跳过标题行
            for (int j = 1; j < RowNum; j++) {
                student = new Student();
                Row row = sheet.getRow(j);
                // 得到单元格数
                // int physicalNumberOfCells = row.getPhysicalNumberOfCells();
                String sNo = row.getCell(0).getStringCellValue();
                student.setsNo(StringUtils.trim(sNo));
                String sName = row.getCell(1).getStringCellValue();
                student.setsName(StringUtils.trim(sName));
                student.setsPass(Mytool.getMD5(student.getsNo()));
                String sSex = row.getCell(2).getStringCellValue();
                // 性别
                switch (StringUtils.trim(sSex)) {
                case "男":
                case Cnst.MALE:
                case "男性":
                    student.setsSex(Cnst.MALE);
                    break;
                default:
                    student.setsSex(Cnst.FEMALE);
                    break;
                }
                String sType = row.getCell(3).getStringCellValue();
                // 类型
                switch (StringUtils.trim(sType)) {
                case "在校":
                case Cnst.STUDENT_TYPE_ATSCHOOL:
                    student.setsType(Cnst.STUDENT_TYPE_ATSCHOOL);
                    break;
                case "退学":
                case Cnst.STUDENT_TYPE_OUTSCHOOL:
                    student.setsType(Cnst.STUDENT_TYPE_OUTSCHOOL);
                    break;

                default:
                    student.setsType(Cnst.STUDENT_TYPE_LEAVESCHOOL);
                    break;
                }
                Integer classId = new Integer(row.getCell(4).getStringCellValue());
                student.setClassId(classId);
                log.info("读取第{}张sheet,第{}行数据:{}", i, j, student.getsNo() + student.getsName());
                Students.add(student);
                if (Students.size() % 100 == 0) {
                    // 100条插入一次
                    count += studentService.insertBatch(Students);
                    Students.clear();
                }
            }

        }
        if (!CollectionUtils.isEmpty(Students)) {
            count += studentService.insertBatch(Students);
        }
        log.info("新增{}条学生数据!", count);
        return count;
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * AjaxResult
     *
     */
    @DeleteMapping
    public AjaxResult delete(@RequestBody List<Integer> ids) {
        int delete = studentService.delete(ids);
        return new AjaxResult(HttpStatus.OK.value(), "成功删除" + delete + "条记录！", null);
    }

    /**
     * 修改学生类型
     * @param ids
     * @param type
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/changeType/{type}")
    public AjaxResult changeType(@RequestBody List<Integer> ids, @PathVariable("type") final String type) {
        int changeType = studentService.changeType(ids, type);
        return new AjaxResult(HttpStatus.OK.value(), changeType + "条记录,修改类型成功！", null);
    }

    @PutMapping("/update")
    public AjaxResult update(@RequestBody Student student) {
        studentService.update(student);
        return new AjaxResult(HttpStatus.OK.value(), "修改成功！", null);
    }

    /**
     * 重置密码
     * @param sNos
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/restpw")
    public AjaxResult resetPassword(@RequestBody List<String> sNos) {
        int count = studentService.resetPassword(sNos);
        return new AjaxResult(HttpStatus.OK.value(), count + "名同学重置密码成功！", null);
    }

    /**
     * 修改密码
     * @param oldPw
     * @param newPw
     * @param newPw2
     * @param session
     * @return
     * AjaxResult
     *
     */
    @PutMapping("/changepw")
    public AjaxResult changePassword(String oldPw, String newPw, String newPw2, HttpSession session) {
        Student student = (Student) session.getAttribute(Cnst.LOGINUSER);
        log.info("学生:{}执行操作修改密码操作。", student.getsNo());
        Student s = new Student();
        s.setsId(student.getsId());
        List<Student> students = studentService.select(s);
        if (StringUtils.isBlank(oldPw) || !StringUtils.equals(Mytool.getMD5(oldPw), students.get(0).getsPass())) {
            log.info("原始密码错误！");
            return AjaxResult.fail("原始密码错误！");
        }
        if (StringUtils.isBlank(newPw) || !StringUtils.equals(newPw, newPw2)) {
            log.info("新密码不一致");
            return AjaxResult.fail("新密码不一致");
        }
        s.setsPass(newPw);
        int update = studentService.update(s);
        if (update > 0) {
            log.info("sessionId:{}学生:{}修改密码成功！", session.getId(), students.get(0).getsNo());
            // 清空session,重新登录
            session.removeAttribute(Cnst.LOGINUSER);
            return AjaxResult.Success("修改密码成功！需重新登录。");
        }
        return AjaxResult.fail("修改失败，稍后再试！");
    }

    /**
     * 查询
     * @param student
     * @param page
     * @return
     * AjaxResult
     *
     */
    @PostMapping
    public AjaxResult select(@RequestBody Student student) {
        List<Student> list = studentService.select(student);
        return new AjaxResult(HttpStatus.OK.value(), null, list);
    }

    /**
     * 查询评教列表
     * @return
     * AjaxResult
     *
     */
    @PostMapping("/eval/{sId}")
    public AjaxResult selectEvaluate(@PathVariable("sId") Integer sId) {
        Student s = new Student();
        s.setsId(sId);
        List<Student> select = studentService.select(s);
        if (CollectionUtils.isEmpty(select)) {
            log.info("session中用户信息失效！");
            throw new NullPointerException("登录信息失效！");
        }
        List<StudentEvaluateVo> selectEvaluate = studentService.selectEvaluate(select.get(0));
        return AjaxResult.Success(null, selectEvaluate);
    }

    @PostMapping("/exit")
    public void exit(HttpSession session) {
        Student student = (Student) session.getAttribute(Cnst.LOGINUSER);
        log.info("session{},学生:{}退出登录！", session.getId(), student.getsName());
        session.invalidate();
    }
}
