package cn.group.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Evaluate;
import cn.group.service.EvaluateService;
import cn.group.utils.AjaxResult;
import cn.group.vo.EvaluateVo;

@RestController
@RequestMapping("/evl")
@CrossOrigin(origins = "*", maxAge = 3600)
public class EvaluateController {

    @Autowired
    private EvaluateService evaluateService;

    @PostMapping("/add")
    public AjaxResult insert(@RequestBody Evaluate evaluate) {
        int insert = evaluateService.insert(evaluate);
        if (insert > 0)
            return AjaxResult.Success("添加问卷成功", insert);
        else
            return AjaxResult.fail("添加失败!");
    }

    @PutMapping
    public AjaxResult update(@RequestBody Evaluate evaluate) {
        int update = evaluateService.update(evaluate);
        if (update > 0)
            return AjaxResult.Success("修改问卷成功");
        else
            return AjaxResult.fail("修改失败!");
    }

    @DeleteMapping
    public AjaxResult delete(@RequestBody List<Integer> ids) {
        int delete = evaluateService.delete(ids);
        return AjaxResult.Success("成功删除" + delete + "条评价指标");
    }

    @PostMapping("/{type}")
    public AjaxResult evaluateTree(@PathVariable String type) {
        List<EvaluateVo> evaluateTree = evaluateService.evaluateTree(0, type);
        return AjaxResult.Success(null, evaluateTree);
    }
}
