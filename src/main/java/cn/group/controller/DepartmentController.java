package cn.group.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.group.entity.Department;
import cn.group.service.DepartmentService;
import cn.group.utils.AjaxResult;

@RestController
@RequestMapping("/dpt")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/insert")
    public AjaxResult insert(@RequestBody Department dpt) {
        int insert = departmentService.insert(dpt);
        if (insert > 0) {
            return new AjaxResult(HttpStatus.OK.value(), "成功", null);
        } else
            return new AjaxResult(HttpStatus.BAD_REQUEST.value(), "失败", null);
    }

    @DeleteMapping
    public AjaxResult delete(@RequestBody List<Integer> ids) {
        departmentService.delete(ids);
        return new AjaxResult(HttpStatus.OK.value(), "删除成功！", null);

    }

    @PutMapping
    public AjaxResult UpdateDepartment(@RequestBody Department dpt) {
        int update = departmentService.UpdateDepartment(dpt);
        return new AjaxResult(HttpStatus.OK.value(), update + "条记录,修改类型成功！", null);
    }

    @PostMapping
    public AjaxResult select() {
        List<Department> selectAll = departmentService.selectAll();
        return new AjaxResult(HttpStatus.OK.value(), null, selectAll);
    }
}
