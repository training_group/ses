package cn.group.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.group.entity.Classcourse;

public interface ClasscourseService {

    int insert(Classcourse cC);

    int delete(Classcourse cC);

    PageInfo<Classcourse> selectPage(Classcourse cC, Integer pageNm);

    List<Classcourse> select(Classcourse cC);

}
