package cn.group.service;

import java.util.List;

import cn.group.entity.Student;
import cn.group.vo.StudentEvaluateVo;

public interface StudentService {

    int insert(Student student);

    int delete(List<Integer> ids);

    int update(Student student);

    int changeType(List<Integer> ids, String type);

    List<Student> select(Student student);

    int resetPassword(List<String> sNos);

    int insertBatch(List<Student> students);

    List<Student> login(Student student);

    List<StudentEvaluateVo> selectEvaluate(Student record);
}
