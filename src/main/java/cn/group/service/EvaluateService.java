package cn.group.service;

import java.util.List;

import cn.group.entity.Evaluate;
import cn.group.vo.EvaluateVo;

public interface EvaluateService {

    int insert(Evaluate evaluate);

    int delete(List<Integer> ids);

    int update(Evaluate evaluate);

    List<Evaluate> selectByeParentId(Integer eParentId, String type);

    List<EvaluateVo> evaluateTree(Integer eParentId, String type);

}
