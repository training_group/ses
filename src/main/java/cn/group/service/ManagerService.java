package cn.group.service;

import java.util.List;

import cn.group.entity.Manager;

/**
 * 管理员(Manager)Service
 * @author Shinelon
 * @createDate 2018/09/06
 *
 */
public interface ManagerService {
    /**
     * 查询管理员信息
     * @param manager
     * @return List<Manager> 管理员List
     */
    public List<Manager> SelectManager(Manager manager);

    int updateByPrimaryKey(Manager manager);
}
