
package cn.group.service;

import java.util.List;

import cn.group.entity.ScoreTea;

/**
 * 
 * @author fzy
 * 2018年9月25日 上午10:03:43
 */
public interface ScoreTeaService {

    /**
     * 添加
     * @param scoreTea
     * @return
     * int
     *
     */
    int insertScoreTea(ScoreTea scoreTea);
    
    /**
     * 查询
     * @param scoreTea
     * @return List<ScoreTea>
     */
    List<ScoreTea> SelectScoreTea(ScoreTea scoreTea);
    
    /**
     * 修改
     * @param scoreTea
     * @return int
     */
    int UpdateScoreTea(ScoreTea scoreTea);
    
    /**
     * 删除
     * @param scoreTea
     * @return int
     */
    int DeleteScoreTea(ScoreTea scoreTea);
}