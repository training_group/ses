package cn.group.service;

import java.util.List;

import cn.group.entity.Teacher;
import cn.group.vo.TeacherEvaluateVo;
import cn.group.vo.TeacherInfo;

/**
 * 教师(Teacher)Service
 * @author Shinelon
 * @createDate 2018/09/07
 *
 */
public interface TeacherService {
    /**
     * 查询教师信息
     * @param teacher
     * @return List<TeacherInfo> 教师list
     */
    public List<TeacherInfo> SelectTeacher(Teacher teacher);

    /**
     * 修改教师信息
     * @param teacher
     * @return int
     */
    public int UpdateTeacher(Teacher teacher);

    int insert(Teacher teacher);

    /**
     * 批量修改教师状态
     * @param tIds
     * @param type
     * @return
     * int
     *
     */
    int changeType(List<Integer> tIds, String type);

    /**
     * 批量重置教师密码
     * @param teacher
     * @return int
     */
    int resetPassword(List<String> tNos);

    int delete(List<Integer> ids);

    int insertBatch(List<Teacher> teachers);

    List<TeacherEvaluateVo> selectEvaluate(Teacher teacher);

}
