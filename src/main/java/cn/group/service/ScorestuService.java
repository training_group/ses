package cn.group.service;

import java.util.List;

import cn.group.entity.Scorestu;

/**
 * 分数-教师-学生Service
 * @author Shinelon
 * @createDate 2018/09/13
 */
public interface ScorestuService {
	
	/**
	 * 查询
	 * @param scorestu
	 * @return List<Scorestu>
	 */
	public List<Scorestu> SelectScorestu(Scorestu scorestu);
	
	/**
	 * 添加
	 * @param scorestu
	 * @return int
	 */
	public int InsertScorestu(Scorestu scorestu);
	
	/**
	 * 修改
	 * @param scorestu
	 * @return int
	 */
	public int UpdateScorestu(Scorestu scorestu);
	
	/**
	 * 删除
	 * @param scorestu
	 * @return int
	 */
	public int DeleteScorestu(Scorestu scorestu);
	
}
