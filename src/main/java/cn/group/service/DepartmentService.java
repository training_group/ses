package cn.group.service;

import java.util.List;

import cn.group.entity.Department;

/**
 * 部门(Department)Service
 * @author Shinelon
 * @createDate 2018/09/07
 *
 */

public interface DepartmentService {
    /**
     * 添加部门
     * @param dpt
     * @return  insertSelective
     */
    int insert(Department dpt);

    /**
     * 删除部门
     * @param ids
     * @return
     */
    int delete(List<Integer> ids);

    /**
     * 查询所有部门
     * @param dpt
     * @param pageNum
     * @return
     */

    List<Department> selectAll();

    /**
     * 修改部门信息
     * @param department
     * @return
     */

    int UpdateDepartment(Department department);

    Department selectByprimaryKey(Integer dId);

    Department selectByDname(String dName);

}
