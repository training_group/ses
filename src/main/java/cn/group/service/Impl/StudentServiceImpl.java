package cn.group.service.Impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Student;
import cn.group.entity.StudentExample;
import cn.group.entity.StudentExample.Criteria;
import cn.group.mapper.StudentMapper;
import cn.group.service.StudentService;
import cn.group.utils.Mytool;
import cn.group.vo.StudentEvaluateVo;

@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentMapper studentMapper;

    /**
     * 增加学生
     * @param student
     * @return
     * int
     *
     */
    @Override
    public int insert(Student student) {
        if (StringUtils.isBlank(student.getsNo())) {
            throw new IllegalArgumentException("添加学生失败，学号为空");
        }
        Student s = new Student();
        s.setsNo(student.getsNo());
        List<Student> select = select(s);
        // 已存在
        if (!CollectionUtils.isEmpty(select)) {
            return -1;
        }
        // 默认学号为密码
        student.setsPass(Mytool.getMD5(student.getsNo()));
        int insertSelective = studentMapper.insert(student);
        return insertSelective;
    }

    /**
     * 异步批量插入
     * @param students
     * void
     *
     */
    @Override
//    @Async
    public int insertBatch(List<Student> students) {
        int insertBatch = studentMapper.insertBatch(students);
        return insertBatch;
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * int
     *
     */
    @Override
    public int delete(List<Integer> ids) {
        StudentExample studentExample = new StudentExample();
        studentExample.createCriteria().andSIdIn(ids);
        int count = studentMapper.deleteByExample(studentExample);
        return count;
    }

    /**
     * 通过主键修改信息，选择性修改
     * @param student
     * @return
     * int
     *
     */
    @Override
    public int update(Student student) {
        // 密码不为空，修改前加密
        if (StringUtils.isNotBlank(student.getsPass())) {
            student.setsPass(Mytool.getMD5(student.getsPass()));
        }
        int update = studentMapper.updateByPrimaryKeySelective(student);
        return update;
    }

    /**
     * 批量修改学生类型
     * @param ids
     * @param type 类型
     * @return
     * int
     *
     */
    @Override
    public int changeType(List<Integer> ids, final String type) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new NullPointerException("没有操作项");
        }
        Student student = new Student();
        student.setsType(type);
        StudentExample studentExample = new StudentExample();
        studentExample.createCriteria().andSIdIn(ids);
        int count = studentMapper.updateByExampleSelective(student, studentExample);
        return count;
    }

    /**
     * 批量重置密码
     * @param sNos 学号
     * @return
     * int
     *
     */
    @Override
    public int resetPassword(List<String> sNos) {
        int count = 0;
        if (CollectionUtils.isEmpty(sNos)) {
            log.info("无重置选项");
            throw new IllegalArgumentException("无选项，密码重置失败");
        }
        Student student = new Student();
        StudentExample studentExample = new StudentExample();
        for (String sNo : sNos) {
            log.info("学生{}执行重置密码操作。。", sNo);
            studentExample.clear();
            student.setsPass(Mytool.getMD5(sNo));
            studentExample.createCriteria().andSNoEqualTo(sNo);
            int res = studentMapper.updateByExampleSelective(student, studentExample);
            count += res;
        }
        return count;
    }

    /**
     * 登录
     * @param student
     * @return
     * AjaxResult
     *
     */
    @Override
    public List<Student> login(Student student) {
        String no = student.getsNo();
        if (StringUtils.isBlank(no)) {
            log.info("学号为空，登录失败");
            throw new IllegalArgumentException("学号为空，登录失败");
        }
        StudentExample studentExample = new StudentExample();
        studentExample.createCriteria().andSNoEqualTo(no);
        List<Student> students = studentMapper.selectByExample(studentExample);
        return students;
    }

    /**
     * 多条件查询
     * @param student
     * @param pageNum
     * @return
     * PageInfo<Student>
     *
     */
    @Override
    public List<Student> select(Student student) {
        StudentExample studentExample = new StudentExample();
        if (student != null) {
            Criteria createCriteria = studentExample.createCriteria();
            // 学号不为空通过学号查找
            if (student.getsId() != null) {
                createCriteria.andSIdEqualTo(student.getsId());
            }
            if (StringUtils.isNotBlank(student.getsNo())) {
                createCriteria.andSNoEqualTo(student.getsNo());
            }
            // 通过班级查找
            if (student.getClassId() != null) {
                createCriteria.andClassIdEqualTo(student.getClassId());
            }
            // 姓名模糊查找
            if (StringUtils.isNotBlank(student.getsName())) {
                createCriteria.andSNameLike("%" + student.getsName() + "%");
            }
        }
        List<Student> students = studentMapper.selectByExample(studentExample);
        return students;
    }

    /**
     * 查询评教列表
     */
    @Override
    public List<StudentEvaluateVo> selectEvaluate(Student student) {
        List<StudentEvaluateVo> selectEvaluate = studentMapper.selectEvaluate(student);
        return selectEvaluate;
    }
}
