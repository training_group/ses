package cn.group.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import cn.group.entity.Department;
import cn.group.entity.DepartmentExample;
import cn.group.mapper.DepartmentMapper;
import cn.group.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;

    /**
     * 增加部门
     */
    @Override
    public int insert(Department dpt) {
        // TODO Auto-generated method stub
        int insertSelective = departmentMapper.insert(dpt);
        return insertSelective;
    }

    /**
     * 删除部门
     *  @param ids
     * @return
      * int
     */
    @Override
    @CacheEvict(value = "dpt", allEntries = true)
    public int delete(List<Integer> ids) {
        DepartmentExample departmentExample = new DepartmentExample();
        departmentExample.createCriteria().andDIdIn(ids);
        int count = departmentMapper.deleteByExample(departmentExample);
        return count;
    }

    /**
     * 修改学院信息
     @param dpt
     * @return
     * int
     */
    @Override
    @Caching(evict = { @CacheEvict(value = "dpt", key = "'all'"), @CacheEvict(value = "dpt", key = "#p0.dId"),
            @CacheEvict(value = "dpt", key = "#p0.dName") })
    public int UpdateDepartment(Department department) {
        int updateBuPK = 0;
        updateBuPK = departmentMapper.updateByPrimaryKeySelective(department);
        return updateBuPK;
    }

    @Override
    @Cacheable(value = "dpt", key = "'all'")
    public List<Department> selectAll() {
        List<Department> department = departmentMapper.selectByExample(null);
        return department;
    }

    /**
     * 通过主键查找部门
     * @param dId
     * @return
     * Department
     *
     */
    @Override
    @Cacheable(value = "dpt", key = "#p0")
    public Department selectByprimaryKey(Integer dId) {
        Department department = null;
//        department = dMap.get(dId);
//        if (department != null) {
//            return department;
//        } else {
        department = departmentMapper.selectByPrimaryKey(dId);
//        if (department != null) {
//            dMap.put(dId, department);
//            dNameMap.put(department.getdName(), department);
//
//        }
        return department;
//            return null;
//        }
    }

    @Override
    @Cacheable(value = "dpt", key = "#p0")
    public Department selectByDname(String dName) {
        Department department = null;
//        department = dNameMap.get(dName);
//        if (department != null) {
//            return department;
//        } else {
        DepartmentExample example = new DepartmentExample();
        example.createCriteria().andDNameEqualTo(dName);
        List<Department> departments = departmentMapper.selectByExample(example);
//        if (!CollectionUtils.isEmpty(departments)) {
        department = departments.get(0);
//            dMap.put(department.getdId(), department);
//            dNameMap.put(dName, department);
//        }
        return department;
//            return null;
//        }
    }
}
