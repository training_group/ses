package cn.group.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Evaluate;
import cn.group.entity.EvaluateExample;
import cn.group.mapper.EvaluateMapper;
import cn.group.service.EvaluateService;
import cn.group.vo.EvaluateVo;

@Service
public class EvaluateServiceImpl implements EvaluateService {

    private static final Logger log = LoggerFactory.getLogger(EvaluateServiceImpl.class);

    @Autowired
    private EvaluateMapper evaluateMapper;

    /**
     * 增加
     * @param evaluate
     * @return
     * int
     *
     */
    @Override
    @Transactional
    public int insert(Evaluate evaluate) {
        // 父id不为空
        Integer pId = evaluate.geteParentId();
        if (pId != null && pId != 0) {
            // 父级改为非最终项
            Evaluate record = new Evaluate();
            record.seteId(pId);
            record.seteIsend(0);
            evaluateMapper.updateByPrimaryKeySelective(record);
        }

        int res = evaluateMapper.insertSelective(evaluate);
        return res;
    }

    /**
     * 删除
     * @return
     * int
     *
     */
    @Override
    public int delete(List<Integer> ids) {
        int res = 0;
        if (!CollectionUtils.isEmpty(ids)) {
            EvaluateExample example = new EvaluateExample();
            example.createCriteria().andEIdIn(ids);
            example.or().andEParentIdIn(ids);
            res = evaluateMapper.deleteByExample(example);
        }
        return res;
    }

    /**
     * 修改
     * @param evaluate
     * @return
     * int
     *
     */
    @Override
    public int update(Evaluate evaluate) {
        if (evaluate.geteId() == null) {
            log.warn("非法操作，修改评价失败");
            throw new IllegalArgumentException("非法操作！");
        }
        int res = evaluateMapper.updateByPrimaryKeySelective(evaluate);
        return res;
    }

    /**
     * 通过父id查询
     * @param eParentId
     * @param type
     * @return
     * List<Evaluate>
     *
     */
    @Override
    public List<Evaluate> selectByeParentId(Integer eParentId, String type) {
        // 默认查找根评价
        eParentId = eParentId == null ? 0 : eParentId;
        EvaluateExample evaluateExample = new EvaluateExample();
        evaluateExample.createCriteria().andEParentIdEqualTo(eParentId).andTypeEqualTo(type);
        List<Evaluate> Evaluates = evaluateMapper.selectByExample(evaluateExample);
        return Evaluates;
    }

    /**
     * 生成评价树
     * @return
     * List<EvaluateVo>
     *
     */
    @Override
    public List<EvaluateVo> evaluateTree(Integer eParentId, String type) {
        List<EvaluateVo> evaluateVo = new ArrayList<>();
        List<Evaluate> evaluate = selectByeParentId(eParentId, type);
        if (!CollectionUtils.isEmpty(evaluate)) {
            for (Evaluate eva : evaluate) {
                List<EvaluateVo> subEvaluateVo = evaluateTree(eva.geteId(), type);
                evaluateVo.add(new EvaluateVo(eva, subEvaluateVo));
            }
        }
        return evaluateVo;
    }
}
