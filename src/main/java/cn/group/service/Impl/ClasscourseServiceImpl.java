package cn.group.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;

import cn.group.entity.Classcourse;
import cn.group.entity.ClasscourseExample;
import cn.group.entity.ClasscourseExample.Criteria;
import cn.group.mapper.ClasscourseMapper;
import cn.group.service.ClasscourseService;

@Service
public class ClasscourseServiceImpl implements ClasscourseService {

    @Autowired
    private ClasscourseMapper classcourseMapper;

    /**
     * 增加
     * @param cC
     * @return
     * int
     *
     */
    @Override
    public int insert(Classcourse cC) {
        int res = classcourseMapper.insertSelective(cC);
        return res;
    }

    /**
     * 删除
     * @param cC
     * @return
     * int
     *
     */
    @Override
    public int delete(Classcourse cC) {
        int res = 0;
        ClasscourseExample classcourseExample = new ClasscourseExample();
        if (cC != null) {
            Criteria createCriteria = classcourseExample.createCriteria();
            if (cC.getCcId() != null) {
                createCriteria.andCcIdEqualTo(cC.getCcId());
            }
            if (cC.getcId() != null) {
                createCriteria.andCIdEqualTo(cC.getcId());
            }
            if (cC.getClassId() != null) {
                createCriteria.andClassIdEqualTo(cC.getClassId());
            }
            res = classcourseMapper.deleteByExample(classcourseExample);
        }
        return res;
    }

    /**
     * 分页查询
     * @param cC
     * @param pageNm
     * @return
     * PageInfo<Classcourse>
     *
     */
    @Override
    public PageInfo<Classcourse> selectPage(Classcourse cC, Integer pageNm) {
        ClasscourseExample classcourseExample = new ClasscourseExample();
        if (cC != null) {
            Criteria createCriteria = classcourseExample.createCriteria();
            if (cC.getCcId() != null) {
                createCriteria.andCcIdEqualTo(cC.getCcId());
            }
            if (cC.getcId() != null) {
                createCriteria.andCIdEqualTo(cC.getcId());
            }
            if (cC.getClassId() != null) {
                createCriteria.andClassIdEqualTo(cC.getClassId());
            }
        }
        List<Classcourse> classCourses = classcourseMapper.selectByExample(classcourseExample);
        PageInfo<Classcourse> PageCC = new PageInfo<>(classCourses);
        return PageCC;
    }

    @Override
    public List<Classcourse> select(Classcourse cC) {
        ClasscourseExample classcourseExample = new ClasscourseExample();
        if (cC != null) {

            Criteria createCriteria = classcourseExample.createCriteria();
            // 主键不为空，匹配主键
            if (cC.getCcId() != null) {
                createCriteria.andCcIdEqualTo(cC.getCcId());
            }
            // 课程id不为空，匹配课程id
            if (cC.getcId() != null) {
                createCriteria.andCIdEqualTo(cC.getcId());
            }
            // 班级id不为空，匹配班级id
            if (cC.getClassId() != null) {
                createCriteria.andClassIdEqualTo(cC.getClassId());
            }
        }
        List<Classcourse> classCourses = classcourseMapper.selectByExample(classcourseExample);
        return classCourses;
    }

    /**
     * 通过班级id查询课程id
     * @param classId
     * @return
     * List<Integer>
     *
     */
    public List<Integer> selectCourseIdByClassId(Integer classId) {
        return classcourseMapper.selectCourseIdByClassId(classId);
    }
}
