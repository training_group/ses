package cn.group.service.Impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Class;
import cn.group.entity.ClassExample;
import cn.group.entity.ClassExample.Criteria;
import cn.group.mapper.ClassMapper;
import cn.group.service.ClassService;

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    private ClassMapper classmapper;

    /**
     * 增加班级
     * @param c
     * @return
     * int
     *
     */
    @Override
    public int add(Class c) {
        int res = classmapper.insertSelective(c);
        return res;
    }

    /**
     * 批量删除班级
     * @param ids
     * @return
     * int
     *
     */
    @Override
    public int delete(List<Integer> ids) {
        int res = 0;
        if (CollectionUtils.isEmpty(ids)) {
            return res;
        }
        ClassExample example = new ClassExample();
        example.createCriteria().andClassIdIn(ids);
        res = classmapper.deleteByExample(example);
        return res;
    }

    /**
     * 查询班级
     * @param c
     * @param pageNum
     * @return
     * PageInfo<Class>
     *
     */
    @Override
    public List<Class> select(Class c) {
        ClassExample example = new ClassExample();
        if (c != null) {
            Criteria createCriteria = example.createCriteria();
            if (StringUtils.isNotBlank(c.getClassName())) {
                createCriteria.andClassNameLike(c.getClassName() + "%");
            }
            if (StringUtils.isNotBlank(c.getClassType())) {
                createCriteria.andClassTypeEqualTo(c.getClassType());
            }
        }
        List<Class> cs = classmapper.selectByExample(example);
        return cs;
    }
}
