package cn.group.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.group.entity.Teachercourse;
import cn.group.entity.TeachercourseExample;
import cn.group.entity.TeachercourseExample.Criteria;
import cn.group.mapper.TeachercourseMapper;
import cn.group.service.TeachercourseService;

/**
 * 教师-课程关联
 * @author fzy
 * 2018年9月11日 上午11:21:52
 */
@Service
public class TeachercourseServiceImpl implements TeachercourseService {
    @Autowired
    private TeachercourseMapper teachercourseMapper;

    /**
     * 增加教师-课程关联
     * @param teachercourse
     * @return
     * int
     *
     */
    @Override
    public int insert(Teachercourse teachercourse) {
        // TODO lgk
        Integer T_Id = teachercourse.gettId();
        if (T_Id == null) {
            throw new IllegalArgumentException("教师ID不能为空");
        }
        Integer C_Id = teachercourse.getcId();
        if (C_Id == null) {
            throw new IllegalArgumentException("课程ID不能为空");
        }
        int insertSelective = teachercourseMapper.insert(teachercourse);

        return insertSelective;
    }

    /**
     * 通过主键删除关联
     * @param id
     * @return
     * int
     *
     */
    @Override
    public int deleteByprimaryKey(List<Integer> ids) {
        // TODO lgk
        TeachercourseExample teachercourseExample = new TeachercourseExample();
        teachercourseExample.createCriteria().andTcIdIn(ids);
        int count = teachercourseMapper.deleteByExample(teachercourseExample);
        return count;
    }

    /**
     * 查询教师-课程关联
     * @param teachercourse
     * @return
     * List<Teachercourse>
     *
     */
    @Override
    public List<Teachercourse> select(Teachercourse teachercourse) {
        TeachercourseExample example = new TeachercourseExample();
        Criteria createCriteria = example.createCriteria();
        // 课程id不为空，匹配课程id
        if (teachercourse.getcId() != null) {
            createCriteria.andCIdEqualTo(teachercourse.getcId());
        }
        // 主键不为空，匹配主键
        if (teachercourse.getTcId() != null) {
            createCriteria.andTcIdEqualTo(teachercourse.getTcId());
        }
        // 教师id不为空，匹配教师id
        if (teachercourse.gettId() != null) {
            createCriteria.andTIdEqualTo(teachercourse.gettId());
        }
        List<Teachercourse> teacherCourses = teachercourseMapper.selectByExample(example);
        return teacherCourses;
    }

    @Override
    public int delete(Teachercourse teachercourse) {
        int res = 0;
        if (teachercourse != null) {
            TeachercourseExample example = new TeachercourseExample();
            Criteria createCriteria = example.createCriteria();
            if (teachercourse.getTcId() != null) {
                createCriteria.andTcIdEqualTo(teachercourse.getTcId());
            }
            if (teachercourse.getcId() != null) {
                createCriteria.andCIdEqualTo(teachercourse.getcId());
            }
            if (teachercourse.gettId() != null) {
                createCriteria.andTIdEqualTo(teachercourse.gettId());
            }
            res = teachercourseMapper.deleteByExample(example);
        }
        return res;
    }

}
