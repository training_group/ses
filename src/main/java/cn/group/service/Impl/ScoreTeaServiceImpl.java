package cn.group.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.group.entity.ScoreTea;
import cn.group.entity.ScoreTeaExample;
import cn.group.entity.ScoreTeaExample.Criteria;
import cn.group.mapper.ScoreTeaMapper;
import cn.group.service.ScoreTeaService;

@Service
public class ScoreTeaServiceImpl implements ScoreTeaService {
	
    @Autowired
    private ScoreTeaMapper scoreTeaMapper;

    @Override
    public int insertScoreTea(ScoreTea scoreTea) {
        int insert = scoreTeaMapper.insertSelective(scoreTea);
        return insert;
    }

	@Override
	public List<ScoreTea> SelectScoreTea(ScoreTea scoreTea) {
		ScoreTeaExample scoreTeaExample = new ScoreTeaExample();
		if(scoreTea!=null){
			Criteria createCriteria = scoreTeaExample.createCriteria();
			if(scoreTea.getStId()!=null){
				createCriteria.andStIdEqualTo(scoreTea.getStId());
			}
			if(scoreTea.gettId()!=null){
				createCriteria.andTIdEqualTo(scoreTea.gettId());
			}
			if(scoreTea.geteTId()!=null){
				createCriteria.andETIdEqualTo(scoreTea.geteTId());
			}
		}
		List<ScoreTea> scoreTeas=scoreTeaMapper.selectByExample(scoreTeaExample);
		return scoreTeas;
	}

	@Override
	public int UpdateScoreTea(ScoreTea scoreTea) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int DeleteScoreTea(ScoreTea scoreTea) {
		// TODO Auto-generated method stub
		return 0;
	}

}
