package cn.group.service.Impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Classcourse;
import cn.group.entity.Teachercourse;
import cn.group.service.ClasscourseService;
import cn.group.service.TeachercourseService;
import cn.group.vo.CourseVo;

/**
 * 授课管理业务类，实现：教师-班级-课程关联的操作
 * @author fzy
 * 2018年9月11日 上午10:51:54
 */
@Service
public class CourseVoServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(CourseVoServiceImpl.class);

    @Autowired
    private TeachercourseService teachercourseService;

    @Autowired
    private ClasscourseService classcourseService;

    /**
     * 添加授课
     * @param courseVo
     * @return 已存在：-1，成功：>0，失败：0
     * int
     *
     */
    public int insert(CourseVo courseVo) {
        Integer tId = courseVo.getTeacher().gettId();
        if (tId == null) {
            throw new IllegalArgumentException("授课教师不能为空");
        }
        Integer classId = courseVo.getC().getClassId();
        if (classId == null) {
            throw new IllegalArgumentException("授课班级不能为空");
        }
        Integer courseId = courseVo.getCourse().getcId();
        if (courseId == null) {
            throw new IllegalArgumentException("教授课程不能为空");
        }
        // 查询教师是否有教这门课（教师-课程关联）
        Teachercourse TC = new Teachercourse();
        TC.setcId(courseId);
        TC.settId(tId);
        List<Teachercourse> teachercourse = teachercourseService.select(TC);
        // 查询这个班级是否学这门课（班级-课程关联存在）
        Classcourse Cc = new Classcourse();
        Cc.setcId(courseId);
        Cc.setClassId(classId);
        List<Classcourse> Classcourse = classcourseService.select(Cc);
        // 有一边关联不存在，说明可添加该授课
        if (CollectionUtils.isEmpty(teachercourse) || CollectionUtils.isEmpty(Classcourse)) {
            int res = 0;
            if (CollectionUtils.isEmpty(teachercourse)) {
                res += teachercourseService.insert(TC);
            }
            if (CollectionUtils.isEmpty(Classcourse)) {
                res += classcourseService.insert(Cc);
            }
            return res;
        }
        log.info("该课程安排已存在！");
        return -1;
    }

    /**
     * 删除排课
     * @param courseVo
     * @return
     * int
     *
     */
    @Transactional
    public int deleteCourseVo(CourseVo courseVo) {
        int res = 0;
        Classcourse Cc = new Classcourse();
        Cc.setcId(courseVo.getC().getClassId());
        Cc.setClassId(courseVo.getCourse().getcId());
        // 删除班级-课程关联
        res += classcourseService.delete(Cc);
        // 删除教师-课程关联
        Teachercourse TC = new Teachercourse();
        TC.setcId(courseVo.getCourse().getcId());
        TC.settId(courseVo.getTeacher().gettId());
        res += teachercourseService.delete(TC);
        return res;
    }

}
