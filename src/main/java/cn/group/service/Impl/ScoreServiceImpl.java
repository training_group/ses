package cn.group.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Score;
import cn.group.entity.ScoreExample;
import cn.group.entity.ScoreExample.Criteria;
import cn.group.entity.Teacher;
import cn.group.entity.TeacherExample;
import cn.group.mapper.CourseMapper;
import cn.group.mapper.ScoreMapper;
import cn.group.mapper.TeacherMapper;
import cn.group.service.ScoreService;
import cn.group.vo.CourseVo;
import cn.group.vo.ScoreStuVo;
import cn.group.vo.ScoreTeaVo;
import cn.group.vo.StudentEvaluateDetail;

/**
 * ScoreServiceImpl
 * @author Shinelon
 * @createDate 2018/09/12
 */
@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    ScoreMapper scoremapper;

    @Autowired
    CourseMapper coursemapper;

    @Autowired
    TeacherMapper teachermapper;

    @Override
    public List<Score> SelectScore(Score score) {
        ScoreExample scoreExample = new ScoreExample();
        if (score != null) {
            Criteria createCriteria = scoreExample.createCriteria();
            // 添加查询条件
            if (score.getScoreId() != null) {
                createCriteria.andScoreIdEqualTo(score.getScoreId());
            }
            if (score.getScoreNum() != null) {
                createCriteria.andScoreNumEqualTo(score.getScoreNum());
            }
            if (score.gettId() != null) {
                createCriteria.andTIdEqualTo(score.gettId());
            }
        }
        List<Score> scores = scoremapper.selectByExample(scoreExample);
        return scores;
    }

    @Override
    public int InsertScore(Score score) {
        int insert = 0;
        if (score != null) {
            insert = scoremapper.insertSelective(score);
        }
        return insert;
    }

    @Override
    public int UpdateScore(Score score) {
        ScoreExample scoreExample = new ScoreExample();
        int update = 0;
        if (score != null) {
            Criteria createCriteria = scoreExample.createCriteria();
            // 添加条件
            if (score.getScoreId() != null) {
                createCriteria.andScoreIdEqualTo(score.getScoreId());
            }
            update = scoremapper.updateByExampleSelective(score, scoreExample);
        }
        return update;
    }

    @Override
    public int DeleteScore(Score score) {
        ScoreExample scoreExample = new ScoreExample();
        int delete = 0;
        if (score != null) {
            Criteria createCriteria = scoreExample.createCriteria();
            // 添加条件
            if (score.getScoreId() != null) {
                createCriteria.andScoreIdEqualTo(score.getScoreId());
            }
            if (score.getScoreNum() != null) {
                createCriteria.andScoreNumEqualTo(score.getScoreNum());
            }
            if (score.gettId() != null) {
                createCriteria.andTIdEqualTo(score.gettId());
            }
            delete = scoremapper.deleteByExample(scoreExample);
        }
        return delete;
    }

    @Override
    public List<ScoreStuVo> selectScoreVo(CourseVo courseVo) {
        // 查询课程相关信息
        List<CourseVo> selectCourseVo = coursemapper.selectCourseVo(courseVo);
        // 查询对应分数
        List<ScoreStuVo> scoreStuVos = new ArrayList<>(selectCourseVo.size());
        for (CourseVo vo : selectCourseVo) {
            Double avgScore = scoremapper.avgScore(vo.getTeacher().gettId(), vo.getC().getClassName());
            scoreStuVos.add(new ScoreStuVo(vo, avgScore));
        }
        return scoreStuVos;
    }

    @Override
    public List<ScoreTeaVo> selectScoreVo(Integer dptId) {
        TeacherExample example = new TeacherExample();
        example.createCriteria().andDIdEqualTo(dptId);
        List<Teacher> teachers = teachermapper.selectByExample(example);
        if (CollectionUtils.isEmpty(teachers)) {
            return null;
        }
        List<ScoreTeaVo> ScoreTeaVos = new ArrayList<>(teachers.size());
        for (Teacher teacher : teachers) {
            Double avgTeaScore = scoremapper.avgTeaScore(teacher.gettId());
            ScoreTeaVos.add(new ScoreTeaVo(teacher, avgTeaScore));
        }
        return ScoreTeaVos;
    }

    @Override
    public List<StudentEvaluateDetail> studentEvaluateDetail(Integer tId, String classId) {
        List<StudentEvaluateDetail> studentEvaluateDetail = scoremapper.studentEvaluateDetail(tId, classId);
        return studentEvaluateDetail;
    }
}
