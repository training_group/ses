package cn.group.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.group.entity.Score;
import cn.group.entity.Scorestu;
import cn.group.service.ScoreService;
import cn.group.service.ScorestuService;
import cn.group.vo.ScoreTeacherStudentVo;

/**
 * Score-Teacher-Student
 * @author Shinelon
 * @createDate 2018/09/13
 */
@Service
public class ScoreTeaStuVoServiceImpl {

    @Autowired
    ScoreService scoreservice;

    @Autowired
    ScorestuService scorestuservice;

    /**
     * 添加评教信息
     * @param stsVo
     * @return -1:已存在，0:失败，1:成功
     */
    @Transactional
    public int InsertSTSVo(ScoreTeacherStudentVo stsVo) {
        int insert = 0;
        Double scoreNum = stsVo.getScore();
        if (scoreNum == null) {
            throw new IllegalArgumentException("分数不能为空");
        }
        Integer tId = stsVo.gettId();
        if (tId == null) {
            throw new IllegalArgumentException("教师id不能为空");
        }
        Integer sId = stsVo.getsId();
        if (sId == null) {
            throw new IllegalArgumentException("学生id不能为空");
        }
        Scorestu scorestu = new Scorestu();
        scorestu.settId(tId);
        scorestu.setsId(sId);
        if (scorestuservice.SelectScorestu(scorestu).size() <= 0) {// 判断是否已经存在
            Score score = new Score();
            score.setScoreNum(scoreNum);
            score.settId(tId);
            if (scoreservice.InsertScore(score) > 0) {// 向score表添加数据
                // 获得Score id
                Integer scoreId = score.getScoreId();
                scorestu.setSsId(scoreId);// 获得Score id
                if (scorestuservice.InsertScorestu(scorestu) > 0) {// 向score_stu表添加数据
                    insert = 1;
                } else {
                    insert = 0;
                }
            } else {
                insert = 0;
            }
        } else {
            insert = -1;
        }
        return insert;
    }

}
