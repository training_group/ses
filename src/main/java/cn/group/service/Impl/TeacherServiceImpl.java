package cn.group.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import cn.group.entity.Department;
import cn.group.entity.Teacher;
import cn.group.entity.TeacherExample;
import cn.group.entity.TeacherExample.Criteria;
import cn.group.mapper.TeacherMapper;
import cn.group.service.DepartmentService;
import cn.group.service.TeacherService;
import cn.group.utils.Mytool;
import cn.group.vo.TeacherEvaluateVo;
import cn.group.vo.TeacherInfo;

/**
 * 教师(Teacher)ServiceImpl
 * @author Shinelon
 * 
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    private static final Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

    @Autowired
    private TeacherMapper teachermapper;

    @Autowired
    private DepartmentService departmentService;

    @Override
    public int insert(Teacher teacher) {
        int insertSelective = teachermapper.insertSelective(teacher);
        return insertSelective;
    }

    /**
     * 查询
     */
    @Override
    public List<TeacherInfo> SelectTeacher(Teacher teacher) {
        TeacherExample teacherExample = new TeacherExample();
        if (teacher != null) {// 判断teacher（教师）是否有值
            Criteria createCriteria = teacherExample.createCriteria();

            // 判断是否有值，如果有添加这个条件
            if (teacher.gettId() != null) { // 教师id
                createCriteria.andTIdEqualTo(teacher.gettId());
            }
            if (StringUtils.isNotBlank(teacher.gettNo())) { // 教师编号
                createCriteria.andTNoEqualTo(teacher.gettNo());
            }
            if (StringUtils.isNotBlank(teacher.gettName())) { // 教师姓名
                createCriteria.andTNameLike("%" + teacher.gettName() + "%");
            }
            if (StringUtils.isNotBlank(teacher.gettSex())) { // 教师性别
                createCriteria.andTSexEqualTo(teacher.gettSex());
            }
            if (StringUtils.isNotBlank(teacher.gettStatus())) { // 教师状态
                createCriteria.andTStatusEqualTo(teacher.gettStatus());
            }
            if (teacher.getdId() != null) { // 教师所在部门id
                createCriteria.andDIdEqualTo(teacher.getdId());
            }
        }
        List<Teacher> teachers = teachermapper.selectByExample(teacherExample);
        List<TeacherInfo> teacherInfos = new ArrayList<>(teachers.size());
        for (Teacher t : teachers) {
            // 查询教师所属院系
            Department department = departmentService.selectByprimaryKey(t.getdId());
            teacherInfos.add(new TeacherInfo(t, department));
        }
        return teacherInfos;
    }

    /**
     * 批量重置密码
     * @param tNos 工号
     * @return
     * int
     *
     */
    @Override
    public int resetPassword(List<String> tNos) {
        if (CollectionUtils.isEmpty(tNos)) {
            log.info("无重置选项");
            throw new IllegalArgumentException("无选项，密码重置失败");
        }
        int count = 0;
        Teacher teacher = new Teacher();
        TeacherExample teacherExample = new TeacherExample();
        for (String tNo : tNos) {
            log.info("教师{}执行重置密码操作。。", tNo);
            teacherExample.clear();
            teacher.settPass(Mytool.getMD5(tNo));
            teacherExample.createCriteria().andTNoEqualTo(tNo);
            int res = teachermapper.updateByExampleSelective(teacher, teacherExample);
            count += res;
        }
        return count;
    }

    @Override
    public int UpdateTeacher(Teacher teacher) {
        // 密码不为空，修改前加密
        if (teacher.gettId() == null) {
            return 0;
        }
        if (StringUtils.isNotBlank(teacher.gettPass())) {
            teacher.settPass(Mytool.getMD5(teacher.gettPass()));
        }
        int update = teachermapper.updateByPrimaryKeySelective(teacher);
        return update;
    }

    /**
     * 批量修改教师状态
     * @param ids
     * @param type 状态
     * @return
     * int
     *
     */
    @Override
    public int changeType(List<Integer> tIds, final String type) {
        if (CollectionUtils.isEmpty(tIds)) {
            throw new NullPointerException("没有操作项");
        }
        Teacher teacher = new Teacher();
        teacher.settStatus(type);
        TeacherExample teacherExample = new TeacherExample();
        teacherExample.createCriteria().andTIdIn(tIds);
        int count = teachermapper.updateByExampleSelective(teacher, teacherExample);
        return count;
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * int
     *
     */
    @Override
    public int delete(List<Integer> ids) {
        TeacherExample teacherExample = new TeacherExample();
        teacherExample.createCriteria().andTIdIn(ids);
        int count = teachermapper.deleteByExample(teacherExample);
        return count;
    }

    /**
     * 异步批量插入
     * @param students
     * void
     *
     */
    @Override
    public int insertBatch(List<Teacher> teachers) {
        int insertBatch = teachermapper.insertBatch(teachers);
        return insertBatch;
    }

    @Override
    public List<TeacherEvaluateVo> selectEvaluate(Teacher teacher) {
        List<TeacherEvaluateVo> selectEvaluate = teachermapper.selectEvaluate(teacher);
        return selectEvaluate;
    }

}
