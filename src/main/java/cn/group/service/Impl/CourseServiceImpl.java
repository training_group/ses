package cn.group.service.Impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.group.entity.Course;
import cn.group.entity.CourseExample;
import cn.group.entity.CourseExample.Criteria;
import cn.group.mapper.CourseMapper;
import cn.group.service.CourseService;
import cn.group.vo.CourseVo;

/**
 * 课程(implements)
 * @author Shinelon
 *	@createDate 2018/09/11
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseMapper coursemapper;

    @Override
    public int InsertCourse(Course course) {
        if (StringUtils.isBlank(course.getcName()) || StringUtils.isBlank(course.getcType())) {
            throw new IllegalArgumentException("添加新课程失败！");
        }
        int insert = coursemapper.insertSelective(course);
        return insert;
    }

    @Override
    public int UpdateCourse(Course course) {
        CourseExample courseExample = new CourseExample();
        int update = 0;
        if (course != null) {
            Criteria createCriteria = courseExample.createCriteria();
            if (course.getcId() != null) {// 添加条件
                createCriteria.andCIdEqualTo(course.getcId());
            }
        }
        update = coursemapper.updateByExampleSelective(course, courseExample);
        return update;
    }

    @Override
    public List<Course> SelectCourse(Course course) {
        CourseExample courseExample = new CourseExample();
        if (course != null) {
            Criteria createCriteria = courseExample.createCriteria();
            if (course.getcId() != null) {// 课程id
                createCriteria.andCIdEqualTo(course.getcId());
            }
            if (StringUtils.isNotBlank(course.getcName())) {// 课程名是否不为空
                createCriteria.andCNameLike("%" + course.getcName() + "%");
            }
            if (StringUtils.isNotBlank(course.getcType())) {// 课程类型是否不为空
                createCriteria.andCTypeEqualTo(course.getcType());
            }
        }
        List<Course> courses = coursemapper.selectByExample(courseExample);
        return courses;
    }

    @Override
    public int DeleteCourse(Course course) {
        CourseExample courseExample = new CourseExample();
        int delete = 0;
        if (course != null) {
            Criteria createCriteria = courseExample.createCriteria();
            if (course.getcId() != null) {// 课程id
                createCriteria.andCIdEqualTo(course.getcId());
            }
            if (course.getcName() != null) {// 课程名是否不为空
                createCriteria.andCNameEqualTo(course.getcName());
            }
            if (course.getcType() != null) {// 课程类型是否不为空
                createCriteria.andCTypeEqualTo(course.getcType());
            }
        }
        delete = coursemapper.deleteByExample(courseExample);
        return delete;
    }

    @Override
    public List<CourseVo> selectCourseVo(CourseVo courseVo) {
        List<CourseVo> selectCourseVo = coursemapper.selectCourseVo(courseVo);
        return selectCourseVo;
    }

}
