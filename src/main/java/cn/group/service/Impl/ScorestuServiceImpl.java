package cn.group.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.group.entity.Scorestu;
import cn.group.entity.ScorestuExample;
import cn.group.entity.ScorestuExample.Criteria;
import cn.group.mapper.ScorestuMapper;
import cn.group.service.ScorestuService;

/**
 * ScorestuServiceImpl
 * @author Shinelon
 * @createDate 2018/09/13
 */
@Service
public class ScorestuServiceImpl implements ScorestuService {
	
	@Autowired
	ScorestuMapper scorestumapper;

	@Override
	public List<Scorestu> SelectScorestu(Scorestu scorestu) {
		ScorestuExample scorestuExample = new ScorestuExample();
		if(scorestu!=null){
			Criteria createCriteria = scorestuExample.createCriteria();
			if(scorestu.getSsId()!=null){
				createCriteria.andSsIdEqualTo(scorestu.getSsId());
			}
			if(scorestu.getsId()!=null){
				createCriteria.andSIdEqualTo(scorestu.getsId());
			}
			if(scorestu.gettId()!=null){
				createCriteria.andTIdEqualTo(scorestu.gettId());
			}
		}
		List<Scorestu> scorestus=scorestumapper.selectByExample(scorestuExample);
		return scorestus;
	}

	@Override
	public int InsertScorestu(Scorestu scorestu) {
		int insert=0;
		if(scorestu!=null){
			insert=scorestumapper.insertSelective(scorestu);
		}
		return insert;
	}

	@Override
	public int UpdateScorestu(Scorestu scorestu) {
		ScorestuExample scorestuExample = new ScorestuExample();
		int update=0;
		if(scorestu!=null){
			Criteria createCriteria = scorestuExample.createCriteria();
			if(scorestu.getSsId()!=null){
				createCriteria.andSsIdEqualTo(scorestu.getSsId());
			}
			update=scorestumapper.updateByExampleSelective(scorestu, scorestuExample);
		}
		return update;
	}

	@Override
	public int DeleteScorestu(Scorestu scorestu) {
		int delete=0;
		if(scorestu!=null){
			if(scorestu.getSsId()!=null){
				delete=scorestumapper.deleteByPrimaryKey(scorestu.getSsId());
			}
		}
		return delete;
	}
}
