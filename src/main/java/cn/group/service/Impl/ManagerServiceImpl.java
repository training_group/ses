package cn.group.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.group.entity.Manager;
import cn.group.entity.ManagerExample;
import cn.group.entity.ManagerExample.Criteria;
import cn.group.mapper.ManagerMapper;
import cn.group.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    ManagerMapper managermapper;

    @Override
    public List<Manager> SelectManager(Manager manager) {
        ManagerExample managerExample = new ManagerExample();
        if (manager != null) { // 判断管理员（manager）是否有值
            Criteria createCriteria = managerExample.createCriteria();
            if (manager.getmId() != null) { // 判断manager中的mId是否有值，如果有表示要添加这个条件
                createCriteria.andMIdEqualTo(manager.getmId());// Criteria中增加的查询条件 mId
            }
            if (manager.getmAccount() != null) { // 判断manager中的mAccount是否有值，如果有表示要添加这个条件
                createCriteria.andMAccountEqualTo(manager.getmAccount()); // Criteria中增加的查询条件 mAccount
            }
        }
        List<Manager> manages = managermapper.selectByExample(managerExample); // 根据条件查询
        return manages;
    }

    @Override
    public int updateByPrimaryKey(Manager manager) {
        if (manager.getmId() == null) {
            return 0;
        }
        int updateByPK = managermapper.updateByPrimaryKeySelective(manager);
        return updateByPK;
    }

}
