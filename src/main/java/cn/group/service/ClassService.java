package cn.group.service;

import java.util.List;

import cn.group.entity.Class;

public interface ClassService {

    int add(Class c);

    int delete(List<Integer> ids);

    List<Class> select(Class c);

}
