package cn.group.service;

import java.util.List;

import cn.group.entity.Score;
import cn.group.vo.CourseVo;
import cn.group.vo.ScoreStuVo;
import cn.group.vo.ScoreTeaVo;
import cn.group.vo.StudentEvaluateDetail;

/**
 * 计分（service）
 * @author Shinelon
 * @createDate 2018/09/12
 */
public interface ScoreService {

    /**
     * 查询分数信息
     * @param score
     * @return List<Score>
     */
    public List<Score> SelectScore(Score score);

    /**
     * 添加分数信息
     * @param score
     * @return int
     */
    public int InsertScore(Score score);

    /**
     * 修改分数信息
     * @param score
     * @return int 
     */
    public int UpdateScore(Score score);

    /**
     * 删除分数数据
     * @param score
     * @return int
     */
    public int DeleteScore(Score score);

    /**
     * 学生评教结果
     * 课程-班级-教师-评分
     * @param courseVo
     * @return
     * List<ScoreStuVo>
     *
     */
    List<ScoreStuVo> selectScoreVo(CourseVo courseVo);

    /**
     * 学生评教详情
     * @param tId
     * @param classId
     * @return
     * List<StudentEvaluateDetail>
     *
     */
    List<StudentEvaluateDetail> studentEvaluateDetail(Integer tId, String classId);

    /**
     * 教师互评结果
     * 教师-评分
     * @param dptId 院系id
     * @return
     * List<ScoreTeaVo>
     *
     */
    List<ScoreTeaVo> selectScoreVo(Integer dptId);
}
