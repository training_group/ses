package cn.group.service;

import java.util.List;

import cn.group.entity.Teachercourse;

public interface TeachercourseService {

    int insert(Teachercourse teachercourse);

    int delete(Teachercourse teachercourse);

    List<Teachercourse> select(Teachercourse teachercourse);

    int deleteByprimaryKey(List<Integer> ids);

}
