package cn.group.service;

import java.util.List;

import cn.group.entity.Course;
import cn.group.vo.CourseVo;

/**
 * 课程(Service)
 * @author Shinelon
 * @createDate 2018/09/11
 */
public interface CourseService {
    /**
     * 添加课程
     * @param course
     * @return int
     */
    public int InsertCourse(Course course);

    /**
     * 修改课程信息
     * @param course
     * @return
     */
    public int UpdateCourse(Course course);

    /**
     * 查询课程信息
     * @param course
     * @return List<Course>
     */
    public List<Course> SelectCourse(Course course);

    /**
     * 删除课程
     * @param course
     * @return int
     */
    public int DeleteCourse(Course course);

    /**
     * 新增，查询排课列表
     * @param courseVo
     * @return
     * List<CourseVo>
     *
     */
    List<CourseVo> selectCourseVo(CourseVo courseVo);
}
