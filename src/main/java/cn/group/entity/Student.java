/*
*
* Student.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Student {
    /**
     * 学生表ID
     */
    private Integer sId;

    /**
     * 学生学号
     */
    private String sNo;

    /**
     * 学生姓名
     */
    private String sName;

    /**
     * 学生密码
     */
    private String sPass;

    /**
     * 学生性别
     */
    private String sSex;

    /**
     * 学生类型（1在校2退学3休学）
     */
    private String sType;

    /**
     * 班级id
     */
    private Integer classId;

    /**
     * 学生表ID
     * @return S_Id 学生表ID
     */
    public Integer getsId() {
        return sId;
    }

    /**
     * 学生表ID
     * @param sId 学生表ID
     */
    public void setsId(Integer sId) {
        this.sId = sId;
    }

    /**
     * 学生学号
     * @return S_No 学生学号
     */
    public String getsNo() {
        return sNo;
    }

    /**
     * 学生学号
     * @param sNo 学生学号
     */
    public void setsNo(String sNo) {
        this.sNo = sNo == null ? null : sNo.trim();
    }

    /**
     * 学生姓名
     * @return S_Name 学生姓名
     */
    public String getsName() {
        return sName;
    }

    /**
     * 学生姓名
     * @param sName 学生姓名
     */
    public void setsName(String sName) {
        this.sName = sName == null ? null : sName.trim();
    }

    /**
     * 学生密码
     * @return S_Pass 学生密码
     */
    public String getsPass() {
        return sPass;
    }

    /**
     * 学生密码
     * @param sPass 学生密码
     */
    public void setsPass(String sPass) {
        this.sPass = sPass == null ? null : sPass.trim();
    }

    /**
     * 学生性别
     * @return S_Sex 学生性别
     */
    public String getsSex() {
        return sSex;
    }

    /**
     * 学生性别
     * @param sSex 学生性别
     */
    public void setsSex(String sSex) {
        this.sSex = sSex == null ? null : sSex.trim();
    }

    /**
     * 学生类型（1在校2退学3休学）
     * @return S_Type 学生类型（1在校2退学3休学）
     */
    public String getsType() {
        return sType;
    }

    /**
     * 学生类型（1在校2退学3休学）
     * @param sType 学生类型（1在校2退学3休学）
     */
    public void setsType(String sType) {
        this.sType = sType == null ? null : sType.trim();
    }

    /**
     * 班级id
     * @return Class_Id 班级id
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * 班级id
     * @param classId 班级id
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }
}