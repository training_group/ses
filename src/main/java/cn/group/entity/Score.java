/*
*
* Score.java
* @Author fengziy
* @date 2018-10-08
*/
package cn.group.entity;

public class Score {
    /**
     * 计分表ID
     */
    private Integer scoreId;

    /**
     * 分数
     */
    private Double scoreNum;

    /**
     * 教师id ;外键-关联教师id
     */
    private Integer tId;

    /**
     * 计分表ID
     * @return Score_Id 计分表ID
     */
    public Integer getScoreId() {
        return scoreId;
    }

    /**
     * 计分表ID
     * @param scoreId 计分表ID
     */
    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    /**
     * 分数
     * @return Score_Num 分数
     */
    public Double getScoreNum() {
        return scoreNum;
    }

    /**
     * 分数
     * @param scoreNum 分数
     */
    public void setScoreNum(Double scoreNum) {
        this.scoreNum = scoreNum;
    }

    /**
     * 教师id ;外键-关联教师id
     * @return T_Id 教师id ;外键-关联教师id
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * 教师id ;外键-关联教师id
     * @param tId 教师id ;外键-关联教师id
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }
}