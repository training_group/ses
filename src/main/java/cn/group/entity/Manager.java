/*
*
* Manager.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Manager {
    /**
     * 管理员表ID
     */
    private Integer mId;

    /**
     * 管理员账号
     */
    private String mAccount;

    /**
     * 管理员密码
     */
    private String mPass;

    /**
     * 管理员表ID
     * @return M_Id 管理员表ID
     */
    public Integer getmId() {
        return mId;
    }

    /**
     * 管理员表ID
     * @param mId 管理员表ID
     */
    public void setmId(Integer mId) {
        this.mId = mId;
    }

    /**
     * 管理员账号
     * @return M_Account 管理员账号
     */
    public String getmAccount() {
        return mAccount;
    }

    /**
     * 管理员账号
     * @param mAccount 管理员账号
     */
    public void setmAccount(String mAccount) {
        this.mAccount = mAccount == null ? null : mAccount.trim();
    }

    /**
     * 管理员密码
     * @return M_Pass 管理员密码
     */
    public String getmPass() {
        return mPass;
    }

    /**
     * 管理员密码
     * @param mPass 管理员密码
     */
    public void setmPass(String mPass) {
        this.mPass = mPass == null ? null : mPass.trim();
    }
}