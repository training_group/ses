/*
*
* Classcourse.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Classcourse {
    /**
     * 主键ID
     */
    private Integer ccId;

    /**
     * 外键，关联班级表id
     */
    private Integer classId;

    /**
     * 外键，关联课程表id
     */
    private Integer cId;

    /**
     * 主键ID
     * @return CC_Id 主键ID
     */
    public Integer getCcId() {
        return ccId;
    }

    /**
     * 主键ID
     * @param ccId 主键ID
     */
    public void setCcId(Integer ccId) {
        this.ccId = ccId;
    }

    /**
     * 外键，关联班级表id
     * @return Class_Id 外键，关联班级表id
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * 外键，关联班级表id
     * @param classId 外键，关联班级表id
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    /**
     * 外键，关联课程表id
     * @return C_Id 外键，关联课程表id
     */
    public Integer getcId() {
        return cId;
    }

    /**
     * 外键，关联课程表id
     * @param cId 外键，关联课程表id
     */
    public void setcId(Integer cId) {
        this.cId = cId;
    }
}