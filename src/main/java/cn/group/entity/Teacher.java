/*
*
* Teacher.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Teacher {
    /**
     * 教师表ID
     */
    private Integer tId;

    /**
     * 教师工号
     */
    private String tNo;

    /**
     * 教师姓名
     */
    private String tName;

    /**
     * 教师密码建议md5加密
     */
    private String tPass;

    /**
     * 教师性别（1男0女）
     */
    private String tSex;

    /**
     * 教师状态（1在职0离职）
     */
    private String tStatus;

    /**
     * 部门id 关联系部id
     */
    private Integer dId;

    /**
     * 教师表ID
     * @return T_Id 教师表ID
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * 教师表ID
     * @param tId 教师表ID
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }

    /**
     * 教师工号
     * @return T_No 教师工号
     */
    public String gettNo() {
        return tNo;
    }

    /**
     * 教师工号
     * @param tNo 教师工号
     */
    public void settNo(String tNo) {
        this.tNo = tNo == null ? null : tNo.trim();
    }

    /**
     * 教师姓名
     * @return T_Name 教师姓名
     */
    public String gettName() {
        return tName;
    }

    /**
     * 教师姓名
     * @param tName 教师姓名
     */
    public void settName(String tName) {
        this.tName = tName == null ? null : tName.trim();
    }

    /**
     * 教师密码建议md5加密
     * @return T_Pass 教师密码建议md5加密
     */
    public String gettPass() {
        return tPass;
    }

    /**
     * 教师密码建议md5加密
     * @param tPass 教师密码建议md5加密
     */
    public void settPass(String tPass) {
        this.tPass = tPass == null ? null : tPass.trim();
    }

    /**
     * 教师性别（1男0女）
     * @return T_Sex 教师性别（1男0女）
     */
    public String gettSex() {
        return tSex;
    }

    /**
     * 教师性别（1男0女）
     * @param tSex 教师性别（1男0女）
     */
    public void settSex(String tSex) {
        this.tSex = tSex == null ? null : tSex.trim();
    }

    /**
     * 教师状态（1在职0离职）
     * @return T_Status 教师状态（1在职0离职）
     */
    public String gettStatus() {
        return tStatus;
    }

    /**
     * 教师状态（1在职0离职）
     * @param tStatus 教师状态（1在职0离职）
     */
    public void settStatus(String tStatus) {
        this.tStatus = tStatus == null ? null : tStatus.trim();
    }

    /**
     * 部门id 关联系部id
     * @return D_Id 部门id 关联系部id
     */
    public Integer getdId() {
        return dId;
    }

    /**
     * 部门id 关联系部id
     * @param dId 部门id 关联系部id
     */
    public void setdId(Integer dId) {
        this.dId = dId;
    }
}