/*
*
* Evaluate.java
* @Author fengziy
* @date 2018-09-21
*/
package cn.group.entity;

public class Evaluate {
    /**
     * 评教ID
     */
    private Integer eId;

    /**
     * 评教标题
     */
    private String eTitle;

    /**
     * 评教父id关联父级id，即本表的E_Id（顶层的父id为0）
     */
    private Integer eParentId;

    /**
     * 类型
     */
    private String type;

    /**
     * 是否最终项（0否1是），只有最终项才在后面生成评分项
     */
    private Integer eIsend;

    /**
     * 评教ID
     * @return E_Id 评教ID
     */
    public Integer geteId() {
        return eId;
    }

    /**
     * 评教ID
     * @param eId 评教ID
     */
    public void seteId(Integer eId) {
        this.eId = eId;
    }

    /**
     * 评教标题
     * @return E_Title 评教标题
     */
    public String geteTitle() {
        return eTitle;
    }

    /**
     * 评教标题
     * @param eTitle 评教标题
     */
    public void seteTitle(String eTitle) {
        this.eTitle = eTitle == null ? null : eTitle.trim();
    }

    /**
     * 评教父id关联父级id，即本表的E_Id（顶层的父id为0）
     * @return E_Parent_Id 评教父id关联父级id，即本表的E_Id（顶层的父id为0）
     */
    public Integer geteParentId() {
        return eParentId;
    }

    /**
     * 评教父id关联父级id，即本表的E_Id（顶层的父id为0）
     * @param eParentId 评教父id关联父级id，即本表的E_Id（顶层的父id为0）
     */
    public void seteParentId(Integer eParentId) {
        this.eParentId = eParentId;
    }

    /**
     * 类型
     * @return type 类型
     */
    public String getType() {
        return type;
    }

    /**
     * 类型
     * @param type 类型
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 是否最终项（0否1是），只有最终项才在后面生成评分项
     * @return E_IsEnd 是否最终项（0否1是），只有最终项才在后面生成评分项
     */
    public Integer geteIsend() {
        return eIsend;
    }

    /**
     * 是否最终项（0否1是），只有最终项才在后面生成评分项
     * @param eIsend 是否最终项（0否1是），只有最终项才在后面生成评分项
     */
    public void seteIsend(Integer eIsend) {
        this.eIsend = eIsend;
    }
}