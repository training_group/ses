/*
*
* Course.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Course {
    /**
     * 课程ID
     */
    private Integer cId;

    /**
     * 课程名称
     */
    private String cName;

    /**
     * 课程类型（1:必修，2:选修）
     */
    private String cType;

    /**
     * 课程ID
     * @return C_Id 课程ID
     */
    public Integer getcId() {
        return cId;
    }

    /**
     * 课程ID
     * @param cId 课程ID
     */
    public void setcId(Integer cId) {
        this.cId = cId;
    }

    /**
     * 课程名称
     * @return C_Name 课程名称
     */
    public String getcName() {
        return cName;
    }

    /**
     * 课程名称
     * @param cName 课程名称
     */
    public void setcName(String cName) {
        this.cName = cName == null ? null : cName.trim();
    }

    /**
     * 课程类型（1:必修，2:选修）
     * @return C_Type 课程类型（1:必修，2:选修）
     */
    public String getcType() {
        return cType;
    }

    /**
     * 课程类型（1:必修，2:选修）
     * @param cType 课程类型（1:必修，2:选修）
     */
    public void setcType(String cType) {
        this.cType = cType == null ? null : cType.trim();
    }
}