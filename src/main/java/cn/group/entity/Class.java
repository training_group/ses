/*
*
* Class.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Class {
    /**
     * 班级id
     */
    private Integer classId;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 班级类型:1:本科，2:专科，3:高职，4:成教
     */
    private String classType;

    /**
     * 班级id
     * @return Class_Id 班级id
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * 班级id
     * @param classId 班级id
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    /**
     * 班级名称
     * @return Class_Name 班级名称
     */
    public String getClassName() {
        return className;
    }

    /**
     * 班级名称
     * @param className 班级名称
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    /**
     * 班级类型:1:本科，2:专科，3:高职，4:成教
     * @return Class_Type 班级类型:1:本科，2:专科，3:高职，4:成教
     */
    public String getClassType() {
        return classType;
    }

    /**
     * 班级类型:1:本科，2:专科，3:高职，4:成教
     * @param classType 班级类型:1:本科，2:专科，3:高职，4:成教
     */
    public void setClassType(String classType) {
        this.classType = classType == null ? null : classType.trim();
    }
}