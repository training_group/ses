/*
*
* Scorestu.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Scorestu {
    /**
     * 主键ID
     */
    private Integer ssId;

    /**
     * 教师id;外键关联教师id
     */
    private Integer tId;

    /**
     * 学生id;外键关联学生id
     */
    private Integer sId;

    /**
     * 主键ID
     * @return SS_Id 主键ID
     */
    public Integer getSsId() {
        return ssId;
    }

    /**
     * 主键ID
     * @param ssId 主键ID
     */
    public void setSsId(Integer ssId) {
        this.ssId = ssId;
    }

    /**
     * 教师id;外键关联教师id
     * @return T_Id 教师id;外键关联教师id
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * 教师id;外键关联教师id
     * @param tId 教师id;外键关联教师id
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }

    /**
     * 学生id;外键关联学生id
     * @return S_Id 学生id;外键关联学生id
     */
    public Integer getsId() {
        return sId;
    }

    /**
     * 学生id;外键关联学生id
     * @param sId 学生id;外键关联学生id
     */
    public void setsId(Integer sId) {
        this.sId = sId;
    }
}