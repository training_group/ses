/*
*
* Teachercourse.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Teachercourse {
    /**
     * 主键ID
     */
    private Integer tcId;

    /**
     * 教师id
     */
    private Integer tId;

    /**
     * 课程id
     */
    private Integer cId;

    /**
     * 主键ID
     * @return TC_Id 主键ID
     */
    public Integer getTcId() {
        return tcId;
    }

    /**
     * 主键ID
     * @param tcId 主键ID
     */
    public void setTcId(Integer tcId) {
        this.tcId = tcId;
    }

    /**
     * 教师id
     * @return T_Id 教师id
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * 教师id
     * @param tId 教师id
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }

    /**
     * 课程id
     * @return C_Id 课程id
     */
    public Integer getcId() {
        return cId;
    }

    /**
     * 课程id
     * @param cId 课程id
     */
    public void setcId(Integer cId) {
        this.cId = cId;
    }
}