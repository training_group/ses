/*
*
* Department.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

public class Department {
    /**
     * 部门 ID
     */
    private Integer dId;

    /**
     * 部门名称
     */
    private String dName;

    /**
     * 部门 ID
     * @return D_Id 部门 ID
     */
    public Integer getdId() {
        return dId;
    }

    /**
     * 部门 ID
     * @param dId 部门 ID
     */
    public void setdId(Integer dId) {
        this.dId = dId;
    }

    /**
     * 部门名称
     * @return D_name 部门名称
     */
    public String getdName() {
        return dName;
    }

    /**
     * 部门名称
     * @param dName 部门名称
     */
    public void setdName(String dName) {
        this.dName = dName == null ? null : dName.trim();
    }
}