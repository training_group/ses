/*
*
* ScoreTea.java
* @Author fengziy
* @date 2018-09-12
*/
package cn.group.entity;

public class ScoreTea {
    /**
     * 主键，自增
     */
    private Integer stId;

    /**
     * 被评教的教师id
     */
    private Integer tId;

    /**
     * 评教的教师id
     */
    private Integer eTId;

    /**
     * 主键，自增
     * @return ST_Id 主键，自增
     */
    public Integer getStId() {
        return stId;
    }

    /**
     * 主键，自增
     * @param stId 主键，自增
     */
    public void setStId(Integer stId) {
        this.stId = stId;
    }

    /**
     * 被评教的教师id
     * @return T_Id 被评教的教师id
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * 被评教的教师id
     * @param tId 被评教的教师id
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }

    /**
     * 评教的教师id
     * @return E_T_Id 评教的教师id
     */
    public Integer geteTId() {
        return eTId;
    }

    /**
     * 评教的教师id
     * @param eTId 评教的教师id
     */
    public void seteTId(Integer eTId) {
        this.eTId = eTId;
    }
}