/*
*
* EvaluateExample.java
* @Author fengziy
* @date 2018-09-21
*/
package cn.group.entity;

import java.util.ArrayList;
import java.util.List;

public class EvaluateExample {
    /**
     * Evaluate
     */
    protected String orderByClause;

    /**
     * Evaluate
     */
    protected boolean distinct;

    /**
     * Evaluate
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public EvaluateExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-21
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * Evaluate 2018-09-21
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEIdIsNull() {
            addCriterion("E_Id is null");
            return (Criteria) this;
        }

        public Criteria andEIdIsNotNull() {
            addCriterion("E_Id is not null");
            return (Criteria) this;
        }

        public Criteria andEIdEqualTo(Integer value) {
            addCriterion("E_Id =", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotEqualTo(Integer value) {
            addCriterion("E_Id <>", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThan(Integer value) {
            addCriterion("E_Id >", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_Id >=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThan(Integer value) {
            addCriterion("E_Id <", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThanOrEqualTo(Integer value) {
            addCriterion("E_Id <=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdIn(List<Integer> values) {
            addCriterion("E_Id in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotIn(List<Integer> values) {
            addCriterion("E_Id not in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdBetween(Integer value1, Integer value2) {
            addCriterion("E_Id between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotBetween(Integer value1, Integer value2) {
            addCriterion("E_Id not between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andETitleIsNull() {
            addCriterion("E_Title is null");
            return (Criteria) this;
        }

        public Criteria andETitleIsNotNull() {
            addCriterion("E_Title is not null");
            return (Criteria) this;
        }

        public Criteria andETitleEqualTo(String value) {
            addCriterion("E_Title =", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotEqualTo(String value) {
            addCriterion("E_Title <>", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleGreaterThan(String value) {
            addCriterion("E_Title >", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleGreaterThanOrEqualTo(String value) {
            addCriterion("E_Title >=", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLessThan(String value) {
            addCriterion("E_Title <", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLessThanOrEqualTo(String value) {
            addCriterion("E_Title <=", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLike(String value) {
            addCriterion("E_Title like", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotLike(String value) {
            addCriterion("E_Title not like", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleIn(List<String> values) {
            addCriterion("E_Title in", values, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotIn(List<String> values) {
            addCriterion("E_Title not in", values, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleBetween(String value1, String value2) {
            addCriterion("E_Title between", value1, value2, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotBetween(String value1, String value2) {
            addCriterion("E_Title not between", value1, value2, "eTitle");
            return (Criteria) this;
        }

        public Criteria andEParentIdIsNull() {
            addCriterion("E_Parent_Id is null");
            return (Criteria) this;
        }

        public Criteria andEParentIdIsNotNull() {
            addCriterion("E_Parent_Id is not null");
            return (Criteria) this;
        }

        public Criteria andEParentIdEqualTo(Integer value) {
            addCriterion("E_Parent_Id =", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotEqualTo(Integer value) {
            addCriterion("E_Parent_Id <>", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdGreaterThan(Integer value) {
            addCriterion("E_Parent_Id >", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_Parent_Id >=", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdLessThan(Integer value) {
            addCriterion("E_Parent_Id <", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("E_Parent_Id <=", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdIn(List<Integer> values) {
            addCriterion("E_Parent_Id in", values, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotIn(List<Integer> values) {
            addCriterion("E_Parent_Id not in", values, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdBetween(Integer value1, Integer value2) {
            addCriterion("E_Parent_Id between", value1, value2, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("E_Parent_Id not between", value1, value2, "eParentId");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andEIsendIsNull() {
            addCriterion("E_IsEnd is null");
            return (Criteria) this;
        }

        public Criteria andEIsendIsNotNull() {
            addCriterion("E_IsEnd is not null");
            return (Criteria) this;
        }

        public Criteria andEIsendEqualTo(Integer value) {
            addCriterion("E_IsEnd =", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendNotEqualTo(Integer value) {
            addCriterion("E_IsEnd <>", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendGreaterThan(Integer value) {
            addCriterion("E_IsEnd >", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_IsEnd >=", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendLessThan(Integer value) {
            addCriterion("E_IsEnd <", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendLessThanOrEqualTo(Integer value) {
            addCriterion("E_IsEnd <=", value, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendIn(List<Integer> values) {
            addCriterion("E_IsEnd in", values, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendNotIn(List<Integer> values) {
            addCriterion("E_IsEnd not in", values, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendBetween(Integer value1, Integer value2) {
            addCriterion("E_IsEnd between", value1, value2, "eIsend");
            return (Criteria) this;
        }

        public Criteria andEIsendNotBetween(Integer value1, Integer value2) {
            addCriterion("E_IsEnd not between", value1, value2, "eIsend");
            return (Criteria) this;
        }
    }

    /**
     *  * Evaluate
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * Evaluate 2018-09-21
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}