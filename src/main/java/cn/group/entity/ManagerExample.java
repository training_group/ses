/*
*
* ManagerExample.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.entity;

import java.util.ArrayList;
import java.util.List;

public class ManagerExample {
    /**
     * Manager
     */
    protected String orderByClause;

    /**
     * Manager
     */
    protected boolean distinct;

    /**
     * Manager
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public ManagerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated 2018-09-06
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * Manager 2018-09-06
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMIdIsNull() {
            addCriterion("M_Id is null");
            return (Criteria) this;
        }

        public Criteria andMIdIsNotNull() {
            addCriterion("M_Id is not null");
            return (Criteria) this;
        }

        public Criteria andMIdEqualTo(Integer value) {
            addCriterion("M_Id =", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotEqualTo(Integer value) {
            addCriterion("M_Id <>", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThan(Integer value) {
            addCriterion("M_Id >", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("M_Id >=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThan(Integer value) {
            addCriterion("M_Id <", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThanOrEqualTo(Integer value) {
            addCriterion("M_Id <=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdIn(List<Integer> values) {
            addCriterion("M_Id in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotIn(List<Integer> values) {
            addCriterion("M_Id not in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdBetween(Integer value1, Integer value2) {
            addCriterion("M_Id between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotBetween(Integer value1, Integer value2) {
            addCriterion("M_Id not between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMAccountIsNull() {
            addCriterion("M_Account is null");
            return (Criteria) this;
        }

        public Criteria andMAccountIsNotNull() {
            addCriterion("M_Account is not null");
            return (Criteria) this;
        }

        public Criteria andMAccountEqualTo(String value) {
            addCriterion("M_Account =", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotEqualTo(String value) {
            addCriterion("M_Account <>", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountGreaterThan(String value) {
            addCriterion("M_Account >", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountGreaterThanOrEqualTo(String value) {
            addCriterion("M_Account >=", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLessThan(String value) {
            addCriterion("M_Account <", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLessThanOrEqualTo(String value) {
            addCriterion("M_Account <=", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLike(String value) {
            addCriterion("M_Account like", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotLike(String value) {
            addCriterion("M_Account not like", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountIn(List<String> values) {
            addCriterion("M_Account in", values, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotIn(List<String> values) {
            addCriterion("M_Account not in", values, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountBetween(String value1, String value2) {
            addCriterion("M_Account between", value1, value2, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotBetween(String value1, String value2) {
            addCriterion("M_Account not between", value1, value2, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMPassIsNull() {
            addCriterion("M_Pass is null");
            return (Criteria) this;
        }

        public Criteria andMPassIsNotNull() {
            addCriterion("M_Pass is not null");
            return (Criteria) this;
        }

        public Criteria andMPassEqualTo(String value) {
            addCriterion("M_Pass =", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotEqualTo(String value) {
            addCriterion("M_Pass <>", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassGreaterThan(String value) {
            addCriterion("M_Pass >", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassGreaterThanOrEqualTo(String value) {
            addCriterion("M_Pass >=", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLessThan(String value) {
            addCriterion("M_Pass <", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLessThanOrEqualTo(String value) {
            addCriterion("M_Pass <=", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLike(String value) {
            addCriterion("M_Pass like", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotLike(String value) {
            addCriterion("M_Pass not like", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassIn(List<String> values) {
            addCriterion("M_Pass in", values, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotIn(List<String> values) {
            addCriterion("M_Pass not in", values, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassBetween(String value1, String value2) {
            addCriterion("M_Pass between", value1, value2, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotBetween(String value1, String value2) {
            addCriterion("M_Pass not between", value1, value2, "mPass");
            return (Criteria) this;
        }
    }

    /**
     *  * Manager
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * Manager 2018-09-06
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}