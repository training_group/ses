package cn.group.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.group.entity.Manager;
import cn.group.entity.Student;
import cn.group.entity.Teacher;
import cn.group.utils.Cnst;
import cn.group.utils.UserVerify;
import cn.group.vo.UserEnum;

/**
 * 用户权限验证拦截器解析器
 * @author fzy
 * 2018年8月17日 上午9:19:43
 */
@Component
public class UserVerifyInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(UserVerifyInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        HandlerMethod hdl = null;
        UserVerify token = null;
        if (handler instanceof HandlerMethod) {
            hdl = (HandlerMethod) handler;
            // 获取方法上的注解
            token = hdl.getMethodAnnotation(UserVerify.class);
            // 获取类上的注解
            if (token == null) {
                token = hdl.getClass().getAnnotation(UserVerify.class);
            }
            // 存在注解那就进行验证
            if (token != null) {
                // 获取注解值
                UserEnum[] value = token.value();
                // 获取session的登录用户
                HttpSession session = request.getSession();
                Object attribute = session.getAttribute(Cnst.LOGINUSER);
                if (attribute == null) {
                    throw new IllegalAccessException("非法访问,未登录或登录信息失效！");
                }
                for (UserEnum userEnum : value) {
                    switch (userEnum) {
                    case ADMIN:
                        if (attribute instanceof Manager) {
                            log.info("session:{}的管理员权限验证通过！", session.getId());
                            return true;
                        }
                        break;
                    case TEACHER:
                        if (attribute instanceof Teacher) {
                            log.info("session:{}的教师权限验证通过！", session.getId());
                            return true;
                        }
                        break;
                    case STUDENT:
                        if (attribute instanceof Student) {
                            log.info("session:{}的学生权限验证通过！", session.getId());
                            return true;
                        }
                        break;
                    default:
                        break;
                    }
                }
                log.info("session:{}的用户权限不足！", session.getId());
                throw new IllegalAccessException("权限不足！");
            }
        }
        return true;
    }
}
