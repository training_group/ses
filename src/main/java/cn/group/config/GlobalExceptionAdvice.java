package cn.group.config;

import java.util.concurrent.TimeoutException;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import cn.group.utils.AjaxResult;

/**
 * 全局异常处理
 * @author fzy
 * 2018年8月17日 上午9:11:16
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    /**
     * 参数错误：400
     *
     * AjaxResult
     * @param e
     * @return
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    public AjaxResult IllegalArgumentExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
    }

    /**
     * 
     * 空指针异常：400
     * AjaxResult
     * @param e
     * @return
     */
    @ExceptionHandler(value = NullPointerException.class)
    public AjaxResult NullPointerExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
    }

    /**
     * 请求超时：408
     * @param e
     * @return
     * AjaxResult
     *
     */
    @ExceptionHandler(value = TimeoutException.class)
    public AjaxResult TimeoutExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.REQUEST_TIMEOUT.value(), "请求超时", null);
    }

    /**
     * 
     * 请求未经授权：401
     * AjaxResult
     * @param e
     * @return
     */
    @ExceptionHandler(value = IllegalAccessException.class)
    public AjaxResult IllegalAccessExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.UNAUTHORIZED.value(), e.getMessage(), null);
    }

    /**
     * 417
     * sql唯一约束异常
     * @param e
     * @return
     * AjaxResult
     *
     */
    @ExceptionHandler(value = DuplicateKeyException.class)
    public AjaxResult DuplicateKeyException(Exception e) {
        return new AjaxResult(HttpStatus.EXPECTATION_FAILED.value(), "该条记录已存在", null);
    }

    /**
     * 503
     * @param e
     * @return
     * AjaxResult
     *
     */
    @ExceptionHandler(value = RuntimeException.class)
    public AjaxResult RuntimeExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.SERVICE_UNAVAILABLE.value(), "稍后再试，服务器异常！", null);
    }

    /**
     * 服务器异常：500
     *
     * AjaxResult
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public AjaxResult GlobaleExceptionHandler(Exception e) {
        return new AjaxResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
    }

}
