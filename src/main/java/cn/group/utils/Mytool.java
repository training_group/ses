package cn.group.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 工具类
 * @author fzy
 * 2018年9月6日 下午8:23:55
 */
public class Mytool {

    /**
     * md5加密
     *
     * @param str
     * @return String
     * @throws Exception
     */
    public static String getMD5(String str) {
    	if(str==null){
    		return null;
    	}
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(str.getBytes());
        return new BigInteger(1, md.digest()).toString(16);

    }

    /**
     * 此时系统时间yyyy-MM-dd HH:mm:ss格式字符串
     * 
     * @return String
     */
    public static String getPubtime() {
        Date time = new Date();
        SimpleDateFormat pubtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return pubtime.format(time);
    }

    /**
     * 此时系统时间yyyyMMddHHmmss格式字符串
     * 
     * @return
     */
    public static String getTimeCode() {
        Date time = new Date();
        SimpleDateFormat timecode = new SimpleDateFormat("yyyyMMddHHmmss");
        return timecode.format(time);
    }

}
