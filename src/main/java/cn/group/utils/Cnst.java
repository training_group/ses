package cn.group.utils;

/**
 * 常量类
 * @author fzy
 * 2018年9月6日 下午7:38:39
 */
public class Cnst {

    /**
     * 班级类型：本科：1
     */
    public static final String CLASS_TYPE_UNDERGRADUATE = "1";
    /**
     * 班级类型：专科：2
     */
    public static final String CLASS_TYPE_SPECIALTY = "2";
    /**
     * 班级类型：高职：3
     */
    public static final String CLASS_TYPE_VOCATIONAL = "3";
    /**
     * 班级类型：成教：4
     */
    public static final String CLASS_TYPE_ADULTEDU = "4";

    /**
     * 课程类型：必修：1
     */
    public static final String COURSE_TYPE_MAJOR = "1";
    /**
     * 课程类型：选修：2
     */
    public static final String COURSE_TYPE_ELECTIVE = "2";

    /**
     * 评教最终项：否：0
     */
    public static final String EVALUATE_NOTEND = "0";

    /**
     * 评教最终项：是：1
     */
    public static final String EVALUATE_END = "1";

    /**
     * 男：1
     */
    public static final String MALE = "1";
    /**
     * 女：0
     */
    public static final String FEMALE = "0";

    /**
     * 教师状态：1：在职
     */
    public static final String TEACHER_STATUS_ONJOB = "1";
    /**
     * 教师状态：0：离职
     */
    public static final String TEACHER_STATUS_QUIT = "0";

    /**
     * 学生类型：1：在校
     */
    public static final String STUDENT_TYPE_ATSCHOOL = "1";
    /**
     * 学生类型：2：退学
     */
    public static final String STUDENT_TYPE_OUTSCHOOL = "2";
    /**
     * 学生类型：3：休学
     */
    public static final String STUDENT_TYPE_LEAVESCHOOL = "3";

    /**
     * 默认分页大小：15条记录
     */
    public static final Integer DEFAULT_PAGE_SIZE = 15;

    /**
     * 存放登录用户的KEY
     */
    public static final String LOGINUSER = "LOGINUSER";
}
