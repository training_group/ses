package cn.group.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import cn.group.vo.UserEnum;

/**
 * 用户验证注解
 * @author fzy
 * 2018年8月17日 上午9:13:15
 */
@Documented
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserVerify {

    UserEnum[] value();
}
