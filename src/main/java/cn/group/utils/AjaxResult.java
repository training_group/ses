package cn.group.utils;

import org.springframework.http.HttpStatus;

/**
 * ajax响应结果类
 * @author fzy
 * 2018年7月24日 上午10:22:16
 */
public class AjaxResult {

    /**
     * 状态
     */
    private Integer status;
    /**
     * 信息
     */
    private String msg;
    /**
     * 数据
     */
    private Object data;

    public AjaxResult() {
    }

    public AjaxResult(Integer status, String msg, Object data) {
        super();
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * 成功
     * @param msg
     * @return
     * AjaxResult
     *
     */
    public static AjaxResult Success(String msg) {
        return new AjaxResult(HttpStatus.OK.value(), msg, null);
    }

    /**
     * 成功
     * @param msg
     * @param data
     * @return
     * AjaxResult
     *
     */
    public static AjaxResult Success(String msg, Object data) {
        return new AjaxResult(HttpStatus.OK.value(), msg, data);
    }

    /**
     * 失败
     * @param msg
     * @return
     * AjaxResult
     *
     */
    public static AjaxResult fail(String msg) {
        return new AjaxResult(HttpStatus.BAD_REQUEST.value(), msg, null);
    }

    /**
     * 失败
     * @param msg
     * @param data
     * @return
     * AjaxResult
     *
     */
    public static AjaxResult fail(String msg, Object data) {
        return new AjaxResult(HttpStatus.BAD_REQUEST.value(), msg, data);
    }

}
