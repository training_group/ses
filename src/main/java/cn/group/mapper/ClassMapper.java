/*
*
* ClassMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.group.entity.Class;
import cn.group.entity.ClassExample;

public interface ClassMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(ClassExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(ClassExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer classId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Class record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Class record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Class> selectByExample(ClassExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Class selectByPrimaryKey(Integer classId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Class record, @Param("example") ClassExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Class record, @Param("example") ClassExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Class record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Class record);
}