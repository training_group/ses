/*
*
* StudentMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.group.entity.Student;
import cn.group.entity.StudentExample;
import cn.group.vo.StudentEvaluateVo;

public interface StudentMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(StudentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(StudentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer sId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Student record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Student record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Student> selectByExample(StudentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Student selectByPrimaryKey(Integer sId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Student record, @Param("example") StudentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Student record, @Param("example") StudentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Student record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Student record);

    /**
     * 批量插入
     * @param records
     * @return
     * int
     *
     */
    int insertBatch(List<Student> records);

    /**
     * 评教列表
     * @param record
     * @return
     * List<StudentEvaluateVo>
     *
     */
    List<StudentEvaluateVo> selectEvaluate(Student record);
}