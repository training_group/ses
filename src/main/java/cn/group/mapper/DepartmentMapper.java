/*
*
* DepartmentMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import cn.group.entity.Department;
import cn.group.entity.DepartmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DepartmentMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(DepartmentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(DepartmentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer dId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Department record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Department record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Department> selectByExample(DepartmentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Department selectByPrimaryKey(Integer dId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Department record, @Param("example") DepartmentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Department record, @Param("example") DepartmentExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Department record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Department record);
}