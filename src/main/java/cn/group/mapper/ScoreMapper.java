/*
*
* ScoreMapper.java
* @Author fengziy
* @date 2018-10-08
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.group.entity.Score;
import cn.group.entity.ScoreExample;
import cn.group.vo.StudentEvaluateDetail;

public interface ScoreMapper {
    /**
     *
     * @mbg.generated 2018-10-08
     */
    long countByExample(ScoreExample example);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int deleteByExample(ScoreExample example);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int deleteByPrimaryKey(Integer scoreId);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int insert(Score record);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int insertSelective(Score record);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    List<Score> selectByExample(ScoreExample example);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    Score selectByPrimaryKey(Integer scoreId);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int updateByExampleSelective(@Param("record") Score record, @Param("example") ScoreExample example);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int updateByExample(@Param("record") Score record, @Param("example") ScoreExample example);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int updateByPrimaryKeySelective(Score record);

    /**
     *
     * @mbg.generated 2018-10-08
     */
    int updateByPrimaryKey(Score record);

    /**
     * 学生评教平均分
     * @param tId 教师id
     * @param classId 班级id
     * @return
     * Double
     *
     */
    Double avgScore(@Param("tId") Integer tId, @Param("classId") String classId);

    /**
     * 教师互评平均分
     * @param tId 教师id
     * @return
     * Double
     *
     */
    Double avgTeaScore(@Param("tId") Integer tId);

    /**
     * 学生评教详情
     * @param tId
     * @param classId
     * @return
     * List<StudentEvaluateDetail>
     *
     */
    List<StudentEvaluateDetail> studentEvaluateDetail(@Param("tId") Integer tId, @Param("classId") String classId);

}