/*
*
* ClasscourseMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.group.entity.Classcourse;
import cn.group.entity.ClasscourseExample;

public interface ClasscourseMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(ClasscourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(ClasscourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer ccId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Classcourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Classcourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Classcourse> selectByExample(ClasscourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Classcourse selectByPrimaryKey(Integer ccId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Classcourse record, @Param("example") ClasscourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Classcourse record, @Param("example") ClasscourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Classcourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Classcourse record);

    /**
     * 通过班级id查询课程id
     * @param classId
     * @return
     * List<Integer>
     *
     */
    @Select("select C_Id from Class_Course where Class_Id=#{classId}")
    List<Integer> selectCourseIdByClassId(Integer classId);
}