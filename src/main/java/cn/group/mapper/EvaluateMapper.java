/*
*
* EvaluateMapper.java
* @Author fengziy
* @date 2018-09-21
*/
package cn.group.mapper;

import cn.group.entity.Evaluate;
import cn.group.entity.EvaluateExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EvaluateMapper {
    /**
     *
     * @mbg.generated 2018-09-21
     */
    long countByExample(EvaluateExample example);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int deleteByExample(EvaluateExample example);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int deleteByPrimaryKey(Integer eId);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int insert(Evaluate record);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int insertSelective(Evaluate record);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    List<Evaluate> selectByExample(EvaluateExample example);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    Evaluate selectByPrimaryKey(Integer eId);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int updateByExampleSelective(@Param("record") Evaluate record, @Param("example") EvaluateExample example);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int updateByExample(@Param("record") Evaluate record, @Param("example") EvaluateExample example);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int updateByPrimaryKeySelective(Evaluate record);

    /**
     *
     * @mbg.generated 2018-09-21
     */
    int updateByPrimaryKey(Evaluate record);
}