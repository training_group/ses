/*
*
* ScoreTeaMapper.java
* @Author fengziy
* @date 2018-09-12
*/
package cn.group.mapper;

import cn.group.entity.ScoreTea;
import cn.group.entity.ScoreTeaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScoreTeaMapper {
    /**
     *
     * @mbg.generated 2018-09-12
     */
    long countByExample(ScoreTeaExample example);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int deleteByExample(ScoreTeaExample example);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int deleteByPrimaryKey(Integer stId);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int insert(ScoreTea record);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int insertSelective(ScoreTea record);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    List<ScoreTea> selectByExample(ScoreTeaExample example);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    ScoreTea selectByPrimaryKey(Integer stId);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int updateByExampleSelective(@Param("record") ScoreTea record, @Param("example") ScoreTeaExample example);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int updateByExample(@Param("record") ScoreTea record, @Param("example") ScoreTeaExample example);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int updateByPrimaryKeySelective(ScoreTea record);

    /**
     *
     * @mbg.generated 2018-09-12
     */
    int updateByPrimaryKey(ScoreTea record);
}