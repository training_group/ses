/*
*
* ScorestuMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import cn.group.entity.Scorestu;
import cn.group.entity.ScorestuExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScorestuMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(ScorestuExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(ScorestuExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer ssId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Scorestu record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Scorestu record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Scorestu> selectByExample(ScorestuExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Scorestu selectByPrimaryKey(Integer ssId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Scorestu record, @Param("example") ScorestuExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Scorestu record, @Param("example") ScorestuExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Scorestu record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Scorestu record);
}