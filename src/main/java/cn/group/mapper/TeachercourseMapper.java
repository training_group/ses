/*
*
* TeachercourseMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import cn.group.entity.Teachercourse;
import cn.group.entity.TeachercourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TeachercourseMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(TeachercourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(TeachercourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(TeachercourseExample teachercourseExample);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Teachercourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Teachercourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Teachercourse> selectByExample(TeachercourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Teachercourse selectByPrimaryKey(Integer tcId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Teachercourse record, @Param("example") TeachercourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Teachercourse record, @Param("example") TeachercourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Teachercourse record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Teachercourse record);
}