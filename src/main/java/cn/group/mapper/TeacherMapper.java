/*
*
* TeacherMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.group.entity.Teacher;
import cn.group.entity.TeacherExample;
import cn.group.vo.TeacherEvaluateVo;

public interface TeacherMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(TeacherExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(TeacherExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer tId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Teacher record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Teacher record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Teacher> selectByExample(TeacherExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Teacher selectByPrimaryKey(Integer tId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Teacher record, @Param("example") TeacherExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Teacher record, @Param("example") TeacherExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Teacher record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Teacher record);

    /**
     * 批量插入
     * @param records
     * @return
     * int
     *
     */
    int insertBatch(List<Teacher> records);

    /**
     * 评教列表
     * @param record
     * @return
     * List<TeacherEvaluateVo>
     *
     */
    List<TeacherEvaluateVo> selectEvaluate(Teacher record);
}