/*
*
* ManagerMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import cn.group.entity.Manager;
import cn.group.entity.ManagerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ManagerMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(ManagerExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(ManagerExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer mId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Manager record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Manager record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Manager> selectByExample(ManagerExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Manager selectByPrimaryKey(Integer mId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Manager record, @Param("example") ManagerExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Manager record, @Param("example") ManagerExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Manager record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Manager record);
}