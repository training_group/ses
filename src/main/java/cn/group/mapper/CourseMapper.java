/*
*
* CourseMapper.java
* @Author fengziy
* @date 2018-09-06
*/
package cn.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.group.entity.Course;
import cn.group.entity.CourseExample;
import cn.group.vo.CourseVo;

public interface CourseMapper {
    /**
     *
     * @mbg.generated 2018-09-06
     */
    long countByExample(CourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByExample(CourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int deleteByPrimaryKey(Integer cId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insert(Course record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int insertSelective(Course record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    List<Course> selectByExample(CourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    Course selectByPrimaryKey(Integer cId);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExampleSelective(@Param("record") Course record, @Param("example") CourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByExample(@Param("record") Course record, @Param("example") CourseExample example);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKeySelective(Course record);

    /**
     *
     * @mbg.generated 2018-09-06
     */
    int updateByPrimaryKey(Course record);

    /**
     * 新增，查询排课列表
     * @param courseVo
     * @return
     * List<CourseVo>
     *
     */
    List<CourseVo> selectCourseVo(CourseVo courseVo);
}