package cn.group.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import cn.group.utils.Cnst;

/**
 * 学生类型转换器
 * @author fzy
 * 2018年9月11日 下午3:00:52
 */
public class StudentTypeHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
    }

    @Override
    public String getResult(ResultSet rs, String columnName) throws SQLException {
        String type = rs.getString(columnName);
        return handler(type);
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        String type = rs.getString(columnIndex);
        return handler(type);
    }

    @Override
    public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String type = cs.getString(columnIndex);
        return handler(type);
    }

    private String handler(String type) {
        switch (type) {
        case Cnst.STUDENT_TYPE_ATSCHOOL:
            return "在校";
        case Cnst.STUDENT_TYPE_LEAVESCHOOL:
            return "休学";
        case Cnst.STUDENT_TYPE_OUTSCHOOL:
            return "退学";
        default:
            return "异常";
        }
    }

}
