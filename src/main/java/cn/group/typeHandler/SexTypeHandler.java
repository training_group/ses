package cn.group.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import cn.group.utils.Cnst;

/**
 * 性别类型转换器
 * @author fzy
 * 2018年9月11日 下午3:00:52
 */
public class SexTypeHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
        String sex = "1";
        switch (StringUtils.trim(parameter)) {
        case "男":
        case Cnst.MALE:
        case "男性":
            sex = Cnst.MALE;
            break;
        default:
            sex = Cnst.FEMALE;
            break;
        }
        ps.setString(i, sex);
    }

    @Override
    public String getResult(ResultSet rs, String columnName) throws SQLException {
        String sex = rs.getString(columnName);
        return StringUtils.equals(sex, "1") ? "男" : "女";
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        String sex = rs.getString(columnIndex);
        return StringUtils.equals(sex, "1") ? "男" : "女";
    }

    @Override
    public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String sex = cs.getString(columnIndex);
        return StringUtils.equals(sex, "1") ? "男" : "女";
    }

}
