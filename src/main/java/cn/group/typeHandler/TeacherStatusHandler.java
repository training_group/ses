package cn.group.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import cn.group.utils.Cnst;

/**
 * 性别类型转换器
 * @author fzy
 * 2018年9月11日 下午3:00:52
 */
public class TeacherStatusHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {

    }

    @Override
    public String getResult(ResultSet rs, String columnName) throws SQLException {
        String type = rs.getString(columnName);
        return handler(type);
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        String type = rs.getString(columnIndex);
        return handler(type);
    }

    @Override
    public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String type = cs.getString(columnIndex);
        return handler(type);
    }

    private String handler(String type) {
        switch (type) {
        case Cnst.TEACHER_STATUS_ONJOB:
            return "在职";
        case Cnst.TEACHER_STATUS_QUIT:
            return "离职";
        default:
            return "异常";
        }
    }

}
