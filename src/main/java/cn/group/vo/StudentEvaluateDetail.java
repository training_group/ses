package cn.group.vo;

import cn.group.entity.Student;

/**
 * 学生评教详情
 * @author fzy
 * 2018年9月25日 上午9:12:14
 */
public class StudentEvaluateDetail {

    private Student student;

    private Integer isEval;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getIsEval() {
        return isEval;
    }

    public void setIsEval(Integer isEval) {
        this.isEval = isEval;
    }

}
