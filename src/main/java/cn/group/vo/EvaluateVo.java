package cn.group.vo;

import java.util.List;

import cn.group.entity.Evaluate;

/**
 * 评论vo
 * @author fzy
 * 2018年9月11日 上午9:37:33
 */
public class EvaluateVo {

    /**
     * 当前评价
     */
    private Evaluate evaluate;

    /**
     * 子评价
     */
    private List<EvaluateVo> evaluateVoList;

    public EvaluateVo() {
    }

    public EvaluateVo(Evaluate evaluate, List<EvaluateVo> evaluateVoList) {
        super();
        this.evaluate = evaluate;
        this.evaluateVoList = evaluateVoList;
    }

    public Evaluate getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(Evaluate evaluate) {
        this.evaluate = evaluate;
    }

    public List<EvaluateVo> getEvaluateVoList() {
        return evaluateVoList;
    }

    public void setEvaluateVoList(List<EvaluateVo> evaluateVoList) {
        this.evaluateVoList = evaluateVoList;
    }

}
