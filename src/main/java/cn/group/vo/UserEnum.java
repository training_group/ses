package cn.group.vo;

/**
 * 用户角色枚举
 * @author fzy
 * 2018年9月13日 上午10:21:32
 */
public enum UserEnum {

    /**
     * 管理员
     */
    ADMIN,
    /**
     * 教师
     */
    TEACHER,
    /**
     * 学生
     */
    STUDENT;
}
