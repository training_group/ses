package cn.group.vo;

import cn.group.entity.Class;
import cn.group.entity.Course;
import cn.group.entity.Teacher;

/**
 * 课程vo
 * @author fzy
 * 2018年9月11日 上午10:34:57
 */
public class CourseVo {
    /**
     * 班级
     */
    private Class c;
    /**
     * 课程
     */
    private Course course;
    /**
     * 教师
     */
    private Teacher teacher;

    public CourseVo() {
    }

    public CourseVo(Class c, Course course, Teacher teacher) {
        super();
        this.c = c;
        this.course = course;
        this.teacher = teacher;
    }

    public Class getC() {
        return c;
    }

    public void setC(Class c) {
        this.c = c;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

}
