package cn.group.vo;

import cn.group.entity.Department;
import cn.group.entity.Teacher;

public class TeacherInfo {

    /**
     * 教师
     */
    private Teacher teacher;

    /**
     * 教师所属院系
     */
    private Department department;

    public TeacherInfo() {
    }

    public TeacherInfo(Teacher teacher, Department department) {
        super();
        this.teacher = teacher;
        this.department = department;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

}
