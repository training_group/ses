package cn.group.vo;

/**
 * 评分-教师-教师vo
 * @author fzy
 * 2018年9月13日 上午11:04:49
 */
public class ScoreTeacherTeacherVo {

    /**
     * 被评教教师id
     */
    private Integer tId;

    /**
     *教师id
     */
    private Integer eTId;

    /**
     * 分数
     */
    private Double score;

    public ScoreTeacherTeacherVo() {
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public Integer geteTId() {
        return eTId;
    }

    public void seteTId(Integer eTId) {
        this.eTId = eTId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}
