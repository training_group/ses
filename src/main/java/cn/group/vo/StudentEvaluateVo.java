package cn.group.vo;

import cn.group.entity.Course;
import cn.group.entity.Teacher;

/**
 * 学生评价列表vo
 * @author fzy
 * 2018年9月19日 上午9:07:19
 */
public class StudentEvaluateVo {

    private Teacher teacher;

    private Course course;

    private Integer isEval;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getIsEval() {
        return isEval;
    }

    public void setIsEval(Integer isEval) {
        this.isEval = isEval;
    }

}
