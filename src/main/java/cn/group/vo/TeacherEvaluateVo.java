package cn.group.vo;

import cn.group.entity.Teacher;

/**
 * 教师评价列表vo
 * @author fzy
 * 2018年9月19日 上午9:07:19
 */
public class TeacherEvaluateVo {

    private Teacher teacher;

    private Integer isEval;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Integer getIsEval() {
        return isEval;
    }

    public void setIsEval(Integer isEval) {
        this.isEval = isEval;
    }

}
