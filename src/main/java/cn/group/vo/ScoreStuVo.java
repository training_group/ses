package cn.group.vo;

/**
 * 教师-班级-课程-评分vo
 * @author fzy
 * 2018年9月18日 上午10:04:12
 */
public class ScoreStuVo {

    private CourseVo courseVo;

    private Double num;

    public ScoreStuVo() {
    }

    public ScoreStuVo(CourseVo courseVo, Double num) {
        super();
        this.courseVo = courseVo;
        this.num = num;
    }

    public CourseVo getCourseVo() {
        return courseVo;
    }

    public void setCourseVo(CourseVo courseVo) {
        this.courseVo = courseVo;
    }

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }
}
