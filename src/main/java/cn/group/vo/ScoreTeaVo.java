package cn.group.vo;

import cn.group.entity.Teacher;

/**
 * 教师-评分vo
 * @author fzy
 * 2018年9月25日 上午11:42:28
 */
public class ScoreTeaVo {

    private Teacher teacher;

    private Double num;

    public ScoreTeaVo() {
    }

    public ScoreTeaVo(Teacher teacher, Double num) {
        super();
        this.teacher = teacher;
        this.num = num;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }

}
