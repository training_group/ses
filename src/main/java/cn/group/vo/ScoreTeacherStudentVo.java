package cn.group.vo;

/**
 * 评分-教师-学生vo
 * @author fzy
 * 2018年9月13日 上午11:04:49
 */
public class ScoreTeacherStudentVo {

    /**
     * 被评教教师id
     */
    private Integer tId;

    /**
     * 学生id
     */
    private Integer sId;

    /**
     * 分数
     */
    private Double score;

    public ScoreTeacherStudentVo() {
    }

    public ScoreTeacherStudentVo(Integer tId, Integer sId, Double score) {
        super();
        this.tId = tId;
        this.sId = sId;
        this.score = score;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}
